import SwiftUI


struct ContactSheetView: View {
    
    @Binding
    var navigateChat: Bool
    
    @Binding
    var selectedContact: Contact?
    
    @State
    var contacts: [Contact] = []
    
    @Environment(\.presentationMode) var presentationMode
    
    var body: some View {
        NavigationView {
            ZStack {
                ScrollView(.vertical, showsIndicators: false) {
                    LazyVStack {
                        ForEach(self.contacts){contact in
                            ContactRowView(contact: contact)
                                .contentShape(Rectangle())
                                .onTapGesture {
                                    self.selectedContact = contact
                                    self.navigateChat = true
                                    
                                    self.presentationMode.wrappedValue.dismiss()
                                }
                        }
                    }
                }
            }
            .navigationBarTitle("Choose contact")
        }
        .onAppear {
            self.contacts = BixDb.instance.getContacts(_isAccepted: true)
        }
    }
}

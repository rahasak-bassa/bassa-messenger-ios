import SwiftUI
import ToastUI


struct PreferenceView: View {
    
    @EnvironmentObject var navigationObserver: NavigationObserver

    @State private var onLogout: Bool = false
    
    @Environment(\.presentationMode) var presentationMode
    
    var body: some View {
        ScrollView(.vertical) {
            VStack(alignment: .leading) {
                HStack {
                    Text("Phone")
                        .padding(EdgeInsets(top: 10, leading: 0, bottom: 10, trailing: 0))
                        .frame(alignment: .trailing)
                    Spacer()
                    Text(PreferenceUtil.instance.get(key: PreferenceUtil.PHONE))
                        .padding(EdgeInsets(top: 10, leading: 0, bottom: 10, trailing: 0))
                }
                .padding(EdgeInsets(top: 10, leading: 20, bottom: 10, trailing: 20))
                .overlay(
                    Rectangle()
                        .frame(height: 1)
                        .foregroundColor(Color.gray.opacity(0.2)),
                    alignment: .bottom
                )
                HStack {
                    Text("Email")
                        .padding(EdgeInsets(top: 10, leading: 0, bottom: 10, trailing: 0))
                        .frame(alignment: .trailing)
                    Spacer()
                    Text(PreferenceUtil.instance.get(key: PreferenceUtil.EMAIL))
                        .padding(EdgeInsets(top: 10, leading: 0, bottom: 10, trailing: 0))
                }
                .padding(EdgeInsets(top: 10, leading: 20, bottom: 10, trailing: 20))
                .overlay(
                    Rectangle()
                        .frame(height: 1)
                        .foregroundColor(Color.gray.opacity(0.2)),
                    alignment: .bottom
                )
                HStack {
                    Text("PIN")
                        .padding(EdgeInsets(top: 10, leading: 0, bottom: 10, trailing: 0))
                        .frame(alignment: .trailing)
                    Spacer()
                    Button(action: {
                        print("change password...")
                        gotoChangePassword()
                    }){
                        Text("Change")
                            .padding(EdgeInsets(top: 10, leading: 20, bottom: 10, trailing: 20))
                            .overlay(
                                RoundedRectangle(cornerRadius: 40)
                                    .stroke(Color.black.opacity(0.8), lineWidth: 1)
                            )
                    }
                }
                .padding(EdgeInsets(top: 10, leading: 20, bottom: 10, trailing: 20))
                .overlay(
                    Rectangle()
                        .frame(height: 1)
                        .foregroundColor(Color.gray.opacity(0.2)),
                    alignment: .bottom
                )
                HStack {
                    Text("Exit")
                        .padding(EdgeInsets(top: 10, leading: 0, bottom: 10, trailing: 0))
                        .frame(alignment: .trailing)
                    Spacer()
                    Button(action: {
                        self.onLogout = true
                    }){
                        Text("Logout")
                            .padding(EdgeInsets(top: 10, leading: 20, bottom: 10, trailing: 20))
                            .overlay(
                                RoundedRectangle(cornerRadius: 40)
                                    .stroke(Color.black.opacity(0.8), lineWidth: 1)
                            )
                    }
                }
                .padding(EdgeInsets(top: 10, leading: 20, bottom: 10, trailing: 20))
                .overlay(
                    Rectangle()
                        .frame(height: 1)
                        .foregroundColor(Color.gray.opacity(0.2)),
                    alignment: .bottom
                )
            }
            .alert(isPresented: $onLogout) {
                Alert(
                    title: Text("Logout"),
                    message: Text("Are you sure you want to exist Messanger?"),
                    primaryButton:
                        .default(
                            Text("Yes"),
                            action: {
                                PreferenceUtil.instance.put(key: PreferenceUtil.LOGIN_STATE, value: "")
                                self.navigationObserver.navigation = "login"
                                //self.presentationMode.wrappedValue.dismiss()
                            }
                        ),
                    secondaryButton:
                        .default(
                            Text("No")
                        )
                )
            }
        }
    }
    
    func gotoChangePassword() {
    }
}

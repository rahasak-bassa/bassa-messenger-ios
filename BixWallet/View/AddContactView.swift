import SwiftUI
import ToastUI


struct AddContactView: View {
    
    @State var email: String = ""
    @State var phone: String = ""
    
    @State private var onProgress: Bool = false
    @State private var onError: Bool = false
    @State private var onSuccess: Bool = false
    @State private var reqSuccess: Bool = false
    
    @Environment(\.presentationMode) var presentationMode
    
    @ObservedObject var contactObserver = ContactObserver()
    
    private let prefsEmail = PreferenceUtil.instance.get(key: PreferenceUtil.EMAIL)
    
    var body: some View {
        VStack{
            TextField("Email", text: $email)
                .keyboardType(.emailAddress)
                .autocapitalization(.none)
                .disableAutocorrection(true)
                .padding(.bottom, 5)
                .background(Color.white)
                .overlay(
                    Rectangle()
                        .frame(height: 1)
                        .foregroundColor(Color.gray.opacity(0.2)),
                    alignment: .bottom
                )
                .padding(.bottom, 40)
            
            TextField("Phone no", text: $phone)
                .keyboardType(.phonePad)
                .autocapitalization(.none)
                .disableAutocorrection(true)
                .padding(.bottom, 5)
                .background(Color.white)
                .overlay(
                    Rectangle()
                        .frame(height: 1)
                        .foregroundColor(Color.gray.opacity(0.2)),
                    alignment: .bottom
                )
                .padding(.bottom, 40)
            
            Button(action: {
                print("adding contact...")
                findContact()
            }){
                Text("Add Contact")
                    .font(.headline)
                    .frame(maxWidth: .infinity)
                    .padding()
                    .foregroundColor(.gray)
                    .overlay(
                        RoundedRectangle(cornerRadius: 40)
                            .stroke(Color.blue.opacity(0.2), lineWidth: 2)
                    )
                    .padding(.bottom, 10)
            }
        }
        .toast(isPresented: $onProgress) {
            // finish progress
            if self.reqSuccess {
                self.onSuccess = true
            } else {
                self.onError = true
            }
        } content: {
            ToastView()
                .toastViewStyle(IndefiniteProgressToastViewStyle())
        }
        .toast(isPresented: $onSuccess, dismissAfter: 2.0) {
            // finish dialog
            self.contactObserver.initContacts()
            self.presentationMode.wrappedValue.dismiss()
        } content: {
            ToastView("Successful")
                .toastViewStyle(SuccessToastViewStyle())
        }
        .toast(isPresented: $onError, dismissAfter: 2.0) {
            // finish dialog
        } content: {
            ToastView("Request fail")
                .toastViewStyle(ErrorToastViewStyle())
        }
        .padding(EdgeInsets(top: 0, leading: 40, bottom: 0, trailing: 40))
    }
    
    func addContact() {
        // validate params
        if (email == "" || phone == "") {
            // display error
            self.onError = true
            return
        }
        
        // create contact in db
        let contact = Contact(email:self.email, phone: self.phone)
        if BixDb.instance.createContact(contact: contact) > 0 {
            self.contactObserver.initContacts()
            self.presentationMode.wrappedValue.dismiss()
        } else {
            self.onError = true
        }
    }
    
    func findContact() {
        // validate params
        if (email == "" || phone == "") {
            // display error
            self.onError = true
            return
        }
        
        let onDeviceReply:(Device?) -> () = { device in
            if  device == nil {
                print("error find device")
                
                self.onProgress = false
                self.reqSuccess = false
            } else {
                print("got device " + device!.id)
                
                // save contact
                let contact = Contact(email:self.email, phone: self.phone, pubkey: device!.pubkey)
                saveContact(contact: contact)
                
                self.onProgress = false
                self.reqSuccess = true
            }
        }
        
        DispatchQueue.global(qos: .userInitiated).async {
            self.onProgress = true

            var req = DeviceRequest()
            req.uid = self.email
            req.sender = self.prefsEmail
            req.devID = self.email
            
            MessangerComm.instance.findDevice(req: req, onDeviceReply: onDeviceReply)
        }
    }
    
    func saveContact(contact: Contact) {
        // create contact in db
        if BixDb.instance.createContact(contact: contact) > 0 {
            // todo handle this
        } else {
            self.onError = true
        }
    }
}

import SwiftUI

struct HomeView: View {
    @State var selected = "Messages"
    
    var body: some View {
        NavigationView {
            VStack {
                TabView(selection: $selected) {
                    RecentChatListView()
                        .tabItem {
                            Image(systemName: "message")
                            Text("Messages")
                        }
                        .tag(tabItems[0])
                    ContactListView()
                        .tabItem {
                            Image(systemName: "person")
                            Text("Contacts")
                        }
                        .tag(tabItems[1])
                    PreferenceView()
                        .tabItem {
                            Image(systemName: "gear")
                            Text("Preference")
                        }
                        .tag(tabItems[2])
                }
            }
            .navigationBarTitle(self.selected)
        }
    }
}

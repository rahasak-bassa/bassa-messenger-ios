import Foundation
import SwiftUI

struct SelfieCallView: View {
    
    @Binding
    var contact: Contact;
    
    private let prefsPhone = PreferenceUtil.instance.get(key: PreferenceUtil.PHONE)
    
    @State private var image: Image?

    @Environment(\.presentationMode) var presentationMode
    
    var body: some View {
        ZStack(alignment: .bottom) {
            VStack(alignment: .leading) {
                HStack {
                    VStack(alignment: .leading) {
                        HStack {
                            Text(self.contact.email)
                                .font(.title)
                                .foregroundColor(Color.white)
                        }
                        .padding(.bottom, 2)
                        HStack {
                            Text(self.contact.phone)
                                .font(.headline)
                                .foregroundColor(Color.white)
                        }
                    }
                    Spacer()
                    Image("default_user_icon")
                        .resizable()
                        .frame(width: 60, height: 60)
                        .clipShape(Circle())
                        .overlay(Circle().stroke(Color.gray.opacity(0.2), lineWidth: 1))
                }
                .padding(EdgeInsets(top: 20, leading: 20, bottom: 0, trailing: 20))
                
                Spacer()
            }
            .background(Color.black)
            .frame(minWidth: 0,
                   maxWidth: .infinity,
                   minHeight: 0,
                   maxHeight: .infinity,
                   alignment: .center
            )
            
            VStack(alignment: .center) {
                Spacer()
                Text("Snap calling...")
                    .font(.subheadline)
                    .foregroundColor(Color.white)
                    .frame(alignment: .center)
                Spacer()
            }
            
            Button(action: {
                // end call
                self.presentationMode.wrappedValue.dismiss()
            }, label: {
                Image(systemName: "phone.down").font(.largeTitle)
                    //.padding(20)
                    .frame(width: 70, height: 70)
                    .background(Color.red)
                    .foregroundColor(.white)
                    .clipShape(Circle())
            })
            
            if image != nil {
                VStack(alignment: .leading) {
                    image?
                        .resizable()
                        .aspectRatio(contentMode: .fill)
                }
                .background(Color.black)
                .frame(minWidth: 0,
                       maxWidth: .infinity,
                       minHeight: 0,
                       maxHeight: .infinity,
                       alignment: .center
                )
            }
        }
        .onAppear {
            requestSelfie()
            
            // subscribe to selfie notification
            NotificationCenter.default.addObserver(forName: BassaNotificationManager.selfieNotification, object: nil, queue: .main) { notification in
                // get selfie image from notification and disply
                let msg = notification.userInfo!["MESSAGE"] as! Message
                let uiImage64 = ImageUtil.instance.decodeBase64(imageDataEncoded: msg.body)
                image = Image(uiImage: uiImage64)
            }
        }
    }
    
    func requestSelfie() {
        let onReply:(Reply) -> () = { reply in
            print("got status " + reply.status)
        }
        
        DispatchQueue.global(qos: .userInitiated).async {
            var msg = Message()
            msg.uid = self.prefsPhone + String(DateUtil.instance.getTimestamp())
            msg.type = "SELFIE_CALL"
            msg.sender = prefsPhone
            msg.receiver = self.contact.phone
            
            MessangerComm.instance.sendMessage(message: msg, onReply: onReply)
        }
    }
}

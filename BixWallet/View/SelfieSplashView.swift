import SwiftUI
import AVFoundation

struct SelfieSplashView: View {
    
    @State var image: UIImage?
    
    @ObservedObject
    var selfiCallObserver = SelfieCallObserver()
        
    @State var didTapCapture: Bool = false
    
    var body: some View {
        ZStack(alignment: .bottom) {
            SplashCameraRepresentable(image: self.$image, didTapCapture: $didTapCapture)
            
            VStack(alignment: .leading) {
                HStack {
                    VStack(alignment: .leading) {
                        HStack {
                            Text("Bix Wallet")
                                .font(.title)
                                .foregroundColor(Color.black.opacity(0))
                        }
                        .padding(.bottom, 2)
                        HStack {
                            Text("One world, one currency")
                                .font(.headline)
                                .foregroundColor(Color.black.opacity(0))
                        }
                    }
                    Spacer()
//                    Image("bixo")
//                        .resizable()
//                        .frame(width: 100, height: 100)
//                        .clipShape(Circle())
                }
                .padding(EdgeInsets(top: 40, leading: 20, bottom: 0, trailing: 0))
                Spacer()
            }
            .background(Color.white.opacity(0.3))
            .frame(minWidth: 0,
                   maxWidth: .infinity,
                   minHeight: 0,
                   maxHeight: .infinity,
                   alignment: .center
            )
            
//            VStack {
//                Spacer()
//                Image("bixo")
//                    .resizable()
//                    .frame(width: 120, height: 120)
//                    .clipShape(Circle())
//                Spacer()
//            }
            
            VStack {
                Button(action: {
                    self.didTapCapture = true
                }){
                    Text("SNAP TO CONTINUE")
                        .font(.headline)
                        .frame(minWidth: 0, maxWidth: .infinity)
                        .padding(.all, 20)
                        .background(Color.black.opacity(0.8))
                        //.cornerRadius(40)
                        .foregroundColor(.white)
                        .overlay(
                            RoundedRectangle(cornerRadius: 0)
                                .stroke(Color.black.opacity(0.8), lineWidth: 1)
                        )
                }
            }
            .padding(EdgeInsets(top: 0, leading: 0, bottom: 0, trailing: 0))
            
//            VStack {
//                if self.navigate {
//                    // goto login
//                    LoginView()
//                }
//            }
        }.edgesIgnoringSafeArea(.all)
    }
}

struct SplashCameraRepresentable: UIViewControllerRepresentable {
    @Binding var image: UIImage?
    
    @EnvironmentObject var navigationObserver: NavigationObserver

    @Binding var didTapCapture: Bool
    
    func makeUIViewController(context: Context) -> SplashCameraController {
        let controller = SplashCameraController()
        controller.delegate = context.coordinator
        return controller
    }
    
    func updateUIViewController(_ cameraViewController: SplashCameraController, context: Context) {
        if(self.didTapCapture) {
            cameraViewController.didTapRecord()
        }
    }
    
    func makeCoordinator() -> Coordinator {
        Coordinator(self)
    }
    
    class Coordinator: NSObject, UINavigationControllerDelegate, AVCapturePhotoCaptureDelegate {
        let parent: SplashCameraRepresentable
        
        init(_ parent: SplashCameraRepresentable) {
            self.parent = parent
        }
        
        func photoOutput(_ output: AVCapturePhotoOutput, didFinishProcessingPhoto photo: AVCapturePhoto, error: Error?) {
            parent.didTapCapture = false
            
            if let imageData = photo.fileDataRepresentation() {
                parent.image = UIImage(data: imageData)
                
                // save image
                let rI = parent.image?.resized(withPercentage: 0.2)
                let fI = rI!.flip()
                ImageUtil.instance.saveImg(image: fI!, forKey: ImageUtil.PROFILE_IMAGE)
            }
            
            parent.navigationObserver.navigation = "login"
        }
    }
}

class SplashCameraController: UIViewController {
    var image: UIImage?
    
    var captureSession = AVCaptureSession()
    var backCamera: AVCaptureDevice?
    var frontCamera: AVCaptureDevice?
    var currentCamera: AVCaptureDevice?
    var photoOutput: AVCapturePhotoOutput?
    var cameraPreviewLayer: AVCaptureVideoPreviewLayer?
    
    var delegate: AVCapturePhotoCaptureDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setup()
    }
    
    func setup() {
        setupCaptureSession()
        setupDevice()
        setupInputOutput()
        setupPreviewLayer()
        startRunningCaptureSession()
    }
    
    func setupCaptureSession() {
        captureSession.sessionPreset = AVCaptureSession.Preset.photo
    }
    
    func setupDevice() {
        let deviceDiscoverySession = AVCaptureDevice.DiscoverySession(deviceTypes: [AVCaptureDevice.DeviceType.builtInWideAngleCamera],
                                                                      mediaType: AVMediaType.video,
                                                                      position: AVCaptureDevice.Position.unspecified)
        for device in deviceDiscoverySession.devices {
            switch device.position {
            case AVCaptureDevice.Position.front:
                self.frontCamera = device
            case AVCaptureDevice.Position.back:
                self.backCamera = device
            default:
                break
            }
        }
        
        self.currentCamera = self.frontCamera
    }
    
    func setupInputOutput() {
        do {
            let captureDeviceInput = try AVCaptureDeviceInput(device: currentCamera!)
            captureSession.addInput(captureDeviceInput)
            photoOutput = AVCapturePhotoOutput()
            photoOutput?.setPreparedPhotoSettingsArray([AVCapturePhotoSettings(format: [AVVideoCodecKey: AVVideoCodecType.jpeg])], completionHandler: nil)
            captureSession.addOutput(photoOutput!)
        } catch {
            print(error)
        }
    }
    
    func setupPreviewLayer() {
        self.cameraPreviewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        self.cameraPreviewLayer?.videoGravity = AVLayerVideoGravity.resizeAspectFill
        self.cameraPreviewLayer?.connection?.videoOrientation = AVCaptureVideoOrientation.portrait
        self.cameraPreviewLayer?.frame = self.view.frame
        self.view.layer.insertSublayer(cameraPreviewLayer!, at: 0)
    }
    
    func startRunningCaptureSession(){
        captureSession.startRunning()
    }
    
    func didTapRecord() {
        let settings = AVCapturePhotoSettings()
        photoOutput?.capturePhoto(with: settings, delegate: delegate!)
    }
}

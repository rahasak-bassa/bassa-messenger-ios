import Foundation

import SwiftUI
import Contacts
import ToastUI

struct PhoneBookView: View {
    @Environment(\.presentationMode) var presentationMode
    
    @State private var onProgress: Bool = false
    @State private var onError: Bool = false
    @State private var onSuccess: Bool = false
    @State private var onConfirm: Bool = false
    @State private var reqError: Bool = false
    
    @State private var contacts:[ContactInfo] = []
    @State private var searchText : String = ""
    
    private let prefsPhone = PreferenceUtil.instance.get(key: PreferenceUtil.PHONE)
    @State var name: String = ""
    @State var phone: String = ""
    @State var sessionKey: String = ""

    @Binding var bassaContacts: [Contact]
    
    var body: some View {
        NavigationView {
            VStack {
                SearchBarView(text: $searchText, placeholder: "Type here")
                List {
                    ForEach (self.contacts.filter({ (cont) -> Bool in
                        self.searchText.isEmpty ? true :
                            "\(cont)".lowercased().contains(self.searchText.lowercased())
                    })) { contact in
                        if contact.phoneNumber != nil {
                            PhoneBookRowView(contact: contact)
                                .contentShape(Rectangle())
                                .onTapGesture {
                                    // Handle the item
                                    self.name = contact.firstName + " " + contact.lastName
                                    self.phone = contact.phoneNumber!.replacingOccurrences(of: " ", with: "")
                                    
                                    // cehck contact is existing
                                    if BixDb.instance.getContact(_phone: self.phone) == nil && self.phone != prefsPhone {
                                        UIApplication.shared.windows.forEach { $0.endEditing(true)}
                                        self.onConfirm = true
                                    }
                                }
                        }
                    }
                }
            }
            .navigationTitle("Add contact")
            .onAppear() {
                self.contacts = PhoneBookUtil.instance.getContacts()
                connect()
            }
        }
        .toast(isPresented: $onProgress) {
            // finish progress
            if self.reqError {
                self.onError = true
            } else {
                self.presentationMode.wrappedValue.dismiss()
            }
        } content: {
            ToastView()
                .toastViewStyle(IndefiniteProgressToastViewStyle())
        }
        .toast(isPresented: $onError, dismissAfter: 2.0) {
            // finish dialog
        } content: {
            ToastView("Request fail")
                .toastViewStyle(ErrorToastViewStyle())
        }
        .alert(isPresented: $onConfirm) {
            Alert(
                title: Text("Add contact"),
                message: Text("Are you sure you want to add " + self.name + "?"),
                primaryButton:
                    .default(
                        Text("Yes"),
                        action: {
                            self.onProgress = true
                            getContact()
                        }
                    ),
                secondaryButton:
                    .default(
                        Text("No")
                    )
            )
        }
    }
    
    func initContacts() {
        DispatchQueue.global(qos: .userInitiated).async {
            self.contacts = PhoneBookUtil.instance.getContacts()
            self.onProgress = false
        }
    }
    
    func connect() {
        let onMessage:(Message) -> () = { msg in
            print("got message uid:" + msg.uid)
        }
        
        DispatchQueue.global(qos: .userInitiated).async {
            var connect = Connect()
            connect.uid = self.prefsPhone + String(DateUtil.instance.getTimestamp())
            connect.sender = self.prefsPhone
            
            MessangerComm.instance.stremMessage(connect: connect, onMessage: onMessage)
        }
    }
    
    func getContact() {
        let onDeviceReply:(Device?) -> () = { device in
            if device == nil {
                print("error find device")
                
                self.onProgress = false
                self.reqError = true
            } else {
                print("got device " + device!.id)
                
                // send friend request
                sendFriendRequest(device: device!)
            }
        }
        
        DispatchQueue.global(qos: .userInitiated).async {
            var req = DeviceRequest()
            req.uid = self.prefsPhone
            req.sender = self.prefsPhone
            req.devID = self.phone
            
            MessangerComm.instance.findDevice(req: req, onDeviceReply: onDeviceReply)
        }
    }
    
    func sendFriendRequest(device: Device) {
        let onReply:(Reply) -> () = { name in
            print("got status " + name.status)
            
            // save contact
            let contact = Contact(email: self.phone, phone: self.phone, pubkey: device.pubkey, myRequest: true, isAccepted: false)
            saveContact(contact: contact)
            
            // refresh contacts list
            self.bassaContacts = BixDb.instance.getContacts()
            
            self.onProgress = false
            self.reqError = false
        }
        
        DispatchQueue.global(qos: .userInitiated).async {
            var msg = Message()
            //self.sessionKey = CryptoUtil.instance.encryptRSA(payload: CryptoUtil.instance.getAESKey(), )!
            msg.uid = self.prefsPhone + String(DateUtil.instance.getTimestamp())
            msg.type = "FRIEND_REQUEST"
            msg.body = self.sessionKey
            msg.receiver = self.phone
            msg.sender = prefsPhone
            
            MessangerComm.instance.sendMessage(message: msg, onReply: onReply)
        }
    }
    
    func saveContact(contact: Contact) {
        DispatchQueue.main.async {
            // create contact in db
            if BixDb.instance.createContact(contact: contact) > 0 {
                // todo handle this
            } else {
                //self.onError = true
            }
        }
    }
}

struct PhoneBookRowView: View {
    @State
    var contact: ContactInfo
    
    var body: some View {
        HStack {
            Image(uiImage: UIImage(data: contact.thumbnail ?? Data()) ?? UIImage(imageLiteralResourceName: "default_user_icon"))
                .resizable()
                .frame(width: 40, height: 40)
                .clipShape(Circle())
                .overlay(Circle().stroke(Color.gray.opacity(0.2), lineWidth: 1))
            VStack(alignment: .leading) {
                Text(contact.firstName + " " + contact.lastName)
                    .padding(.bottom, 2)
                Text(contact.phoneNumber ?? "")
                    .font(.subheadline)
                    .foregroundColor(Color.gray)
            }
            Spacer()
        }
        .padding(EdgeInsets(top: 8, leading: 0, bottom: 5, trailing: 0))
    }
}

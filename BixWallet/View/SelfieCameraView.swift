import SwiftUI
import AVFoundation

//struct BassaCameraView: View {
//    @State private var image: Image?
//    @State private var showingCustomCamera = false
//    @State private var inputImage: UIImage?
//
//    var body: some View {
//        NavigationView {
//            VStack {
//                ZStack {
//                    Rectangle().fill(Color.secondary)
//
//                    if image != nil {
//                        image?
//                            .resizable()
//                            .aspectRatio(contentMode: .fill)
//                    } else {
//                        Text("Take Photo").foregroundColor(.white).font(.headline)
//                    }
//                }
//                .onTapGesture {
//                    self.showingCustomCamera = true
//                }
//            }
//            .sheet(isPresented: $showingCustomCamera, onDismiss: loadImage) {
//                SelfieCameraView(image: self.$inputImage)
//            }
//            .edgesIgnoringSafeArea(.all)
//        }
//    }
//
//    func loadImage() {
//        guard let inputImage = inputImage else { return }
//        print("done-------------- same image here")
//        let ri = inputImage.resized(withPercentage: 0.3)
//        let img = Image(uiImage: ri!)
//        ImageUtil.instance.saveImg(image: ri!, forKey: "20112")
//        UIImageWriteToSavedPhotosAlbum(ri!, nil, nil, nil)
//        //UserDefaults.standard.set(inputImage, forKey: "1010")
//    }
//}

struct SelfieCameraView: View {
    
    @Binding
    var image: UIImage?
    
    @Binding
    var selfieCallContact: Contact?
    
    @State var didTapCapture: Bool = false
    
    var body: some View {
        ZStack(alignment: .bottom) {
            CustomCameraRepresentable(image: self.$image, didTapCapture: $didTapCapture)
            CaptureButtonView()
                .onTapGesture {
                    self.didTapCapture = true
                }
            VStack(alignment: .leading) {
                HStack {
                    VStack(alignment: .leading) {
                        HStack {
                            Text(self.selfieCallContact!.email)
                                .font(.title)
                                .foregroundColor(Color.white)
                        }
                        .padding(.bottom, 2)
                        HStack {
                            Text(self.selfieCallContact!.phone)
                                .font(.headline)
                                .foregroundColor(Color.white)
                        }
                    }
                    Spacer()
                    Image("default_user_icon")
                        .resizable()
                        .frame(width: 60, height: 60)
                        .clipShape(Circle())
                        .overlay(Circle().stroke(Color.gray.opacity(0.2), lineWidth: 1))
                }
                .padding(EdgeInsets(top: 20, leading: 20, bottom: 0, trailing: 20))
                Spacer()
            }
            .frame(minWidth: 0,
                   maxWidth: .infinity,
                   minHeight: 0,
                   maxHeight: .infinity,
                   alignment: .center
            )
            
            VStack(alignment: .center) {
                Spacer()
                Text("Snap calling...")
                    .font(.subheadline)
                    .foregroundColor(Color.white)
                    .frame(alignment: .center)
                Spacer()
            }
        }
    }
}

struct CustomCameraRepresentable: UIViewControllerRepresentable {
    @Environment(\.presentationMode) var presentationMode
    @Binding var image: UIImage?
    @Binding var didTapCapture: Bool
    
    func makeUIViewController(context: Context) -> CustomCameraController {
        let controller = CustomCameraController()
        controller.delegate = context.coordinator
        return controller
    }
    
    func updateUIViewController(_ cameraViewController: CustomCameraController, context: Context) {
        if(self.didTapCapture) {
            cameraViewController.didTapRecord()
        }
    }
    
    func makeCoordinator() -> Coordinator {
        Coordinator(self)
    }
    
    class Coordinator: NSObject, UINavigationControllerDelegate, AVCapturePhotoCaptureDelegate {
        let parent: CustomCameraRepresentable
        
        init(_ parent: CustomCameraRepresentable) {
            self.parent = parent
        }
        
        func photoOutput(_ output: AVCapturePhotoOutput, didFinishProcessingPhoto photo: AVCapturePhoto, error: Error?) {
            parent.didTapCapture = false
            
            if let imageData = photo.fileDataRepresentation() {
                parent.image = UIImage(data: imageData)
            }
            
            parent.presentationMode.wrappedValue.dismiss()
        }
    }
}

class CustomCameraController: UIViewController {
    var image: UIImage?
    
    var captureSession = AVCaptureSession()
    var backCamera: AVCaptureDevice?
    var frontCamera: AVCaptureDevice?
    var currentCamera: AVCaptureDevice?
    var photoOutput: AVCapturePhotoOutput?
    var cameraPreviewLayer: AVCaptureVideoPreviewLayer?
    
    var delegate: AVCapturePhotoCaptureDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setup()
    }
    
    func setup() {
        setupCaptureSession()
        setupDevice()
        setupInputOutput()
        setupPreviewLayer()
        startRunningCaptureSession()
    }
    
    func setupCaptureSession() {
        captureSession.sessionPreset = AVCaptureSession.Preset.photo
    }
    
    func setupDevice() {
        let deviceDiscoverySession = AVCaptureDevice.DiscoverySession(deviceTypes: [AVCaptureDevice.DeviceType.builtInWideAngleCamera],
                                                                      mediaType: AVMediaType.video,
                                                                      position: AVCaptureDevice.Position.unspecified)
        for device in deviceDiscoverySession.devices {
            switch device.position {
            case AVCaptureDevice.Position.front:
                self.frontCamera = device
            case AVCaptureDevice.Position.back:
                self.backCamera = device
            default:
                break
            }
        }
        
        self.currentCamera = self.frontCamera
    }
    
    func setupInputOutput() {
        do {
            let captureDeviceInput = try AVCaptureDeviceInput(device: currentCamera!)
            captureSession.addInput(captureDeviceInput)
            photoOutput = AVCapturePhotoOutput()
            photoOutput?.setPreparedPhotoSettingsArray([AVCapturePhotoSettings(format: [AVVideoCodecKey: AVVideoCodecType.jpeg])], completionHandler: nil)
            captureSession.addOutput(photoOutput!)
        } catch {
            print(error)
        }
    }
    
    func setupPreviewLayer() {
        self.cameraPreviewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        self.cameraPreviewLayer?.videoGravity = AVLayerVideoGravity.resizeAspectFill
        self.cameraPreviewLayer?.connection?.videoOrientation = AVCaptureVideoOrientation.portrait
        self.cameraPreviewLayer?.frame = self.view.frame
        self.view.layer.insertSublayer(cameraPreviewLayer!, at: 0)
    }
    
    func startRunningCaptureSession(){
        captureSession.startRunning()
    }
    
    func didTapRecord() {
        let settings = AVCapturePhotoSettings()
        photoOutput?.capturePhoto(with: settings, delegate: delegate!)
    }
}

struct CaptureButtonView: View {
    @State private var animationAmount: CGFloat = 1
    var body: some View {
        Image(systemName: "camera")
            .font(.largeTitle)
            //.padding(25)
            .frame(width: 70, height: 70)
            .background(Color.red)
            .foregroundColor(.white)
            .clipShape(Circle())
            .overlay(
                Circle()
                    .stroke(Color.red)
                    .scaleEffect(animationAmount)
                    .opacity(Double(2 - animationAmount))
                    .animation(Animation.easeOut(duration: 1)
                                .repeatForever(autoreverses: false))
            )
            .onAppear {
                self.animationAmount = 2
            }
    }
}

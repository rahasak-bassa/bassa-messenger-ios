import SwiftUI
import ToastUI
import SSToastMessage


struct LoginView: View {
    
    @State var phone: String = ""
    @State var password: String = ""
    
    @State private var onError: Bool = false
    @State private var regSuccess: Bool = false
    @State private var navigateRegister: Bool = false
    @State private var navigateForgotPassword: Bool = false
    @State private var errorMsg: String = ""
    
    @EnvironmentObject var navigationObserver: NavigationObserver

    @State var showToast = false

    private let prefsPhone = PreferenceUtil.instance.get(key: PreferenceUtil.PHONE)
    private let prefsPassword = PreferenceUtil.instance.get(key: PreferenceUtil.PASSWORD)
    
    var body: some View {
        NavigationView {
            GeometryReader { geometry in
                ScrollView(.vertical, showsIndicators: false) {
                    VStack{
                        TextField("Phone", text: $phone)
                            .keyboardType(.phonePad)
                            .autocapitalization(.none)
                            .disableAutocorrection(true)
                            .padding(.bottom, 5)
                            .background(Color.white)
                            .overlay(
                                Rectangle()
                                    .frame(height: 1)
                                    .foregroundColor(Color.gray.opacity(0.2)),
                                alignment: .bottom
                            )
                            .padding(.bottom, 40)
                        
                        SecureField("PIN", text: $password)
                            .keyboardType(.numberPad)
                            .autocapitalization(.none)
                            .disableAutocorrection(true)
                            .padding(.bottom, 5)
                            .background(Color.white)
                            .overlay(
                                Rectangle()
                                    .frame(height: 1)
                                    .foregroundColor(Color.gray.opacity(0.2)),
                                alignment: .bottom
                            )
                            .padding(.bottom, 50)
                        
                        Button(action: {
                            UIApplication.shared.windows.forEach { $0.endEditing(true)}
                            doLogin()
                            //self.showToast = true
                        }){
                            Text("Login")
                                .font(.headline)
                                .frame(maxWidth: .infinity)
                                .padding()
                                .foregroundColor(.gray)
                                .overlay(
                                    RoundedRectangle(cornerRadius: 40)
                                        .stroke(Color.black.opacity(0.8), lineWidth: 1)
                                )
                                .padding(.bottom, 10)
                        }
                        
                        Button(action: {
                            if(self.prefsPhone == "") {
                                self.navigationObserver.navigation = "register"
                            } else {
                                self.navigateForgotPassword = true
                            }
                        }){
                            Text(self.prefsPhone == "" ? "Don't have an account? Register" : "Forgot PIN?")
                                .font(.subheadline)
                                .foregroundColor(.gray)
                                .padding(.bottom, 5)
                                .overlay(
                                    Rectangle()
                                        .frame(height: 1)
                                        .foregroundColor(Color.gray.opacity(0.2)),
                                    alignment: .bottom
                                )
                        }
                    }
                    .toast(isPresented: $onError, dismissAfter: 2.0) {
                        // finish dialog
                    } content: {
                        ToastView(self.errorMsg)
                            .toastViewStyle(ErrorToastViewStyle())
                    }
                    .padding(EdgeInsets(top: 10, leading: 40, bottom: 10, trailing: 40))
                    .frame(width: geometry.size.width)
                    .frame(minHeight: geometry.size.height)
                    .navigationBarTitle("Login")
                }
                .onTapGesture {
                    UIApplication.shared.windows.forEach { $0.endEditing(true)}
                }
            }
        }
        .present(isPresented: self.$showToast, type: .toast, position: .top) {
            // create your own view for toast
            self.createTopToastView()
        }
        .onAppear {
            self.phone = prefsPhone
            BassaNotificationManager.instance.requestPermission()
        }
    }
    
    func doLogin() {
        let fPhone = PhoneNumberUtil.instance.internationalize(phone: self.phone)
        if fPhone == nil {
            self.onError = true
            self.errorMsg = "Invalid phone"
            return
        }
        phone = fPhone!.replacingOccurrences(of: " ", with: "")
        
        // validate fields
        if (phone == "" || password == "") {
            // display error
            self.onError = true
            self.errorMsg = "Empty fields"
            return
        } else if(phone != self.prefsPhone) {
            // display error
            self.onError = true
            self.errorMsg = "Invalid phone"
            return
        } else if(password != self.prefsPassword) {
            // display error
            self.onError = true
            self.errorMsg = "Invalid PIN"
            return
        }
        
        // valid login
        // goto home
        self.password = ""
        PreferenceUtil.instance.put(key: PreferenceUtil.LOGIN_STATE, value: "LOGIN")
        self.navigationObserver.navigation = "home"
    }
    
    func createTopToastView() -> some View {
        VStack {
            Spacer(minLength: 20)
            HStack() {
                Image("default_user_icon")
                    .resizable()
                    .aspectRatio(contentMode: ContentMode.fill)
                    .frame(width: 50, height: 50)
                    .cornerRadius(25)

                VStack(alignment: .leading, spacing: 2) {
                    HStack {
                        Text("Mike")
                            .foregroundColor(.white)
                            .fontWeight(.bold)
                        Spacer()
                        Text("10:10")
                            .font(.system(size: 12))
                            .foregroundColor(Color(red: 0.9, green: 0.9, blue: 0.9))
                    }

                    Text("New message received.")
                        .lineLimit(2)
                        .font(.system(size: 14))
                        .foregroundColor(.white)
                }
            }.padding(15)
        }
        .frame(width: UIScreen.main.bounds.width, height: 110)
        .background(Color.blue.opacity(0.9))
    }
}

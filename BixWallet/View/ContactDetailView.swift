import SwiftUI
import ToastUI


struct ContactDetailView: View {
    
    @State var contact: Contact
            
    @State private var onSuccess: Bool = false
    @State private var onError: Bool = false
    @State private var reqError: Bool = false
    @State private var onProgress: Bool = false
    @State private var onDelete: Bool = false
    @State private var navigateChat: Bool = false
    @State private var showAcceptRequest: Bool = false
    
    @State var sessionKey: String = ""
    
    private let prefsPhone = PreferenceUtil.instance.get(key: PreferenceUtil.PHONE)
    
    @Environment(\.presentationMode) var presentationMode
    
    var body: some View {
        GeometryReader { reader in
            ScrollView(.vertical) {
                VStack(alignment: .leading) {
                    ZStack {
                        Image(uiImage: UIImage(data: contact.image ?? Data()) ?? UIImage(imageLiteralResourceName: "default_user_icon"))
                            .resizable()
                            .scaledToFill()
                            .frame(height: 280)
                            .clipped()
                        //                        VStack(alignment:.trailing) {
                        //                            Spacer()
                        //                            HStack {
                        //                                Spacer()
                        //                                Button(action: {
                        //                                    //self.navigateChat = true
                        //                                }){
                        //                                    Text("Take snap")
                        //                                        .padding(EdgeInsets(top: 10, leading: 20, bottom: 10, trailing: 20))
                        //                                        .overlay(
                        //                                            RoundedRectangle(cornerRadius: 40)
                        //                                                .stroke(Color.black.opacity(0.8), lineWidth: 1)
                        //                                        )
                        //                                }
                        //                                .padding(EdgeInsets(top: 0, leading: 0, bottom: 10, trailing: 10))
                        //                            }
                        //                        }
                    }
                    .frame(height: 280)
                    HStack {
                        Text("Name")
                            .padding(EdgeInsets(top: 10, leading: 0, bottom: 10, trailing: 0))
                            .frame(alignment: .trailing)
                        Spacer()
                        Text(contact.email)
                            .padding(EdgeInsets(top: 10, leading: 0, bottom: 10, trailing: 0))
                    }
                    .padding(EdgeInsets(top: 10, leading: 20, bottom: 10, trailing: 20))
                    .overlay(
                        Rectangle()
                            .frame(height: 1)
                            .foregroundColor(Color.gray.opacity(0.2)),
                        alignment: .bottom
                    )
                    HStack {
                        Text("Phone")
                            .padding(EdgeInsets(top: 10, leading: 0, bottom: 10, trailing: 0))
                            .frame(alignment: .trailing)
                        Spacer()
                        Text(contact.phone)
                            .padding(EdgeInsets(top: 10, leading: 0, bottom: 10, trailing: 0))
                    }
                    .padding(EdgeInsets(top: 10, leading: 20, bottom: 10, trailing: 20))
                    .overlay(
                        Rectangle()
                            .frame(height: 1)
                            .foregroundColor(Color.gray.opacity(0.2)),
                        alignment: .bottom
                    )
                    
                    if(!self.contact.isAccepted && self.contact.myRequest) {
                        // TODO no need to show, may be show other view
                    } else {
                        HStack {
                            Text(self.contact.isAccepted ? "Send chat" : "Accept friend request")
                                .padding(EdgeInsets(top: 10, leading: 0, bottom: 10, trailing: 0))
                                .frame(alignment: .trailing)
                            Spacer()
                            Button(action: {
                                if (self.contact.isAccepted) {
                                    self.navigateChat = true
                                } else if(self.contact.myRequest) {
                                    // do nothing
                                } else {
                                    self.onProgress = true
                                    getContact()
                                }
                            }){
                                Text(self.contact.isAccepted ? "Chat" : "Accept")
                                    .padding(EdgeInsets(top: 10, leading: 20, bottom: 10, trailing: 20))
                                    .overlay(
                                        RoundedRectangle(cornerRadius: 40)
                                            .stroke(Color.black.opacity(0.8), lineWidth: 1)
                                    )
                            }
                        }
                        .padding(EdgeInsets(top: 10, leading: 20, bottom: 10, trailing: 20))
                        .overlay(
                            Rectangle()
                                .frame(height: 1)
                                .foregroundColor(Color.gray.opacity(0.2)),
                            alignment: .bottom
                        )
                    }
                    
                    HStack {
                        Text("Delete contact")
                            .padding(EdgeInsets(top: 10, leading: 0, bottom: 10, trailing: 0))
                            .frame(alignment: .trailing)
                        Spacer()
                        Button(action: {
                            self.onDelete = true
                        }){
                            Text("Delete")
                                .padding(EdgeInsets(top: 10, leading: 20, bottom: 10, trailing: 20))
                                .overlay(
                                    RoundedRectangle(cornerRadius: 40)
                                        .stroke(Color.black.opacity(0.8), lineWidth: 1)
                                )
                        }
                    }
                    .padding(EdgeInsets(top: 10, leading: 20, bottom: 10, trailing: 20))
                    .overlay(
                        Rectangle()
                            .frame(height: 1)
                            .foregroundColor(Color.gray.opacity(0.2)),
                        alignment: .bottom
                    )
                    NavigationLink(destination: ChatView(contact: contact), isActive: self.$navigateChat) {
                        EmptyView()
                    }
                }
                .toast(isPresented: $onProgress) {
                    // finish progress
                    if self.reqError {
                        self.onError = true
                    } else {
                        self.presentationMode.wrappedValue.dismiss()
                    }
                } content: {
                    ToastView()
                        .toastViewStyle(IndefiniteProgressToastViewStyle())
                }
                .toast(isPresented: $onSuccess, dismissAfter: 2.0) {
                    print("Success toast dismissed")
                } content: {
                    ToastView("Successful")
                        .toastViewStyle(SuccessToastViewStyle())
                }
                .toast(isPresented: $onError, dismissAfter: 2.0) {
                    print("Error toast dismissed")
                } content: {
                    ToastView("Error")
                        .toastViewStyle(ErrorToastViewStyle())
                }
                .alert(isPresented: $onDelete) {
                    Alert(
                        title: Text("Logout"),
                        message: Text("Are you sure you want to delete the contact?"),
                        primaryButton:
                            .default(
                                Text("Yes"),
                                action: {
                                    deleteContact()
                                }
                            ),
                        secondaryButton:
                            .default(
                                Text("No")
                            )
                    )
                }
            }
            .navigationBarTitle("", displayMode: .inline)
        }
    }
    
    func deleteContact() {
        if BixDb.instance.deleteContact(_phone: contact.phone) {
            _ = BixDb.instance.deleteChatsOfContact(_phone: contact.phone)
            
            self.presentationMode.wrappedValue.dismiss()
        } else {
            self.onError = true
        }
    }
    
    func getContact() {
        let onDeviceReply:(Device?) -> () = { device in
            if device == nil {
                print("error find device")
                
                self.onProgress = false
                self.reqError = true
            } else {
                print("got device " + device!.id)
                
                // update device public key
                self.contact.pubkey = device!.pubkey
                _ = BixDb.instance.updateContactPublicKey(_phone: device!.id, _pubkey: self.contact.pubkey)
                
                // accept friend request
                acceptFriendRequest(device: device!)
            }
        }
        
        DispatchQueue.global(qos: .userInitiated).async {
            var req = DeviceRequest()
            req.uid = self.prefsPhone
            req.sender = self.prefsPhone
            req.devID = self.contact.phone
            
            MessangerComm.instance.findDevice(req: req, onDeviceReply: onDeviceReply)
        }
    }
    
    func acceptFriendRequest(device: Device) {
        let onReply:(Reply) -> () = { name in
            print("got status " + name.status)
            
            // accept contact status
            // update session key
            _ = BixDb.instance.markContactAccepted(_phone: device.id)
            _ = BixDb.instance.updateContactSessionKey(_phone: device.id, _seesionKey: self.sessionKey)
            
            self.onProgress = false
            self.reqError = false
        }
        
        DispatchQueue.global(qos: .userInitiated).async {
            self.sessionKey = CryptoUtil.instance.getAESKey()
            let body = CryptoUtil.instance.encryptRSA(payload: sessionKey, keystr: self.contact.pubkey)!
            var msg = Message()
            msg.uid = self.prefsPhone + String(DateUtil.instance.getTimestamp())
            msg.type = "ACCEPT_REQUEST"
            msg.body = body
            msg.receiver = self.contact.phone
            msg.sender = prefsPhone
            
            MessangerComm.instance.sendMessage(message: msg, onReply: onReply)
        }
    }
}

import SwiftUI

import Firebase

@main
struct BixWalletApp: App {
    @UIApplicationDelegateAdaptor(AppDelegate.self) var appDelegate
    let navigationObserver = NavigationObserver()
    
    var body: some Scene {
        WindowGroup {
            SplashView()
                .environmentObject(navigationObserver)
        }
    }
}

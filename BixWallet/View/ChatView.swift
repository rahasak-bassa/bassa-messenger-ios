import SwiftUI

struct Chat: Identifiable, Hashable {
    var id = UUID()
    var uid: String
    var body: String
    var timestamp: Int64
    var contact: Contact
    var myChat: Bool
    var viewed: Bool = false
    var inOrder: Bool = false
}

struct ChatView: View {
    @State
    private var image: Image?
    
    @State
    private var requestSelfie = false
    
    @State
    private var inputImage: UIImage?
    
    @State
    var contact: Contact
    
    @State
    var chatBody: String = ""
    
    @State
    var scroll: Bool = false
    
    @State var chats: [Chat] = []
    
    @State var currentContact: Contact?

    private let prefsPhone = PreferenceUtil.instance.get(key: PreferenceUtil.PHONE)
    
    var body: some View {
        VStack {
            ScrollView(.vertical, showsIndicators: false) {
                LazyVStack {
                    ForEach(self.chats, id: \.self) { chat in
                        ChatRowView(chat: chat)
                            .rotationEffect(.radians(.pi))
                            .scaleEffect(x: -1, y: 1, anchor: .center)
                    }
                    .id(UUID())
                }
            }
            .padding(EdgeInsets(top: 0, leading: 20, bottom: 0, trailing: 20))
            .rotationEffect(.radians(.pi))
            .scaleEffect(x: -1, y: 1, anchor: .center)
            .onTapGesture {
                UIApplication.shared.windows.forEach { $0.endEditing(true)}
            }
            
            HStack(alignment: .bottom) {
                TextEditor(text: $chatBody)
                    //.disableAutocorrection(true)
                    .padding(EdgeInsets(top: 0, leading: 10, bottom: 0, trailing: 10))
//                    .overlay(
//                        Rectangle()
//                            .frame(height: 1)
//                            .foregroundColor(Color.black.opacity(0.8)),
//                        alignment: .bottom
//                    )
                    .overlay(
                        RoundedRectangle(cornerRadius: 40)
                            .stroke(Color.black.opacity(0.8), lineWidth: 1)
                    )
                    .frame(height: 50)

                //Divider()
                //    .frame(height: 25)
                Button(action: {
                    if self.chatBody != "" {
                        sendChat()
                    }
                }, label: {
                    Image(systemName: "paperplane")
                        .frame(width: 18, height: 18)
                        .padding(8)
                        .background(Color.black.opacity(0.8))
                        .foregroundColor(.white)
                        .clipShape(Circle())
                })
                .padding(EdgeInsets(top: 0, leading: 7, bottom: 0, trailing: 0))
                
                //Divider()
                //    .frame(height: 25)
                Button(action: {
                    self.requestSelfie = true
                }, label: {
                    Image(systemName: "camera")
                        .frame(width: 18, height: 18)
                        .padding(8)
                        .background(Color.black.opacity(0.8))
                        .foregroundColor(.white)
                        .clipShape(Circle())
                })
                .padding(EdgeInsets(top: 0, leading: 5, bottom: 0, trailing: 0))
            }
            //.frame(height: 55)
            .padding(EdgeInsets(top: 0, leading: 10, bottom: 10, trailing: 10))
//            .overlay(
//                Rectangle()
//                    .frame(height: 1)
//                    .foregroundColor(Color.gray.opacity(0.2)),
//                alignment: .top
//            )
        }
        .navigationBarTitle(Text(self.contact.email), displayMode: .inline)
        .onAppear {
            _ = BixDb.instance.updateUnreadCount(_contact: contact.phone, count: 0, reset: true)
            self.chats = BixDb.instance.getChats(_phone: self.contact.phone)
            self.currentContact = contact
            
            connect()
        }
        .onDisappear {
            _ = BixDb.instance.updateUnreadCount(_contact: contact.phone, count: 0, reset: true)
            self.currentContact = nil
        }
        .onReceive(NotificationCenter.default.publisher(for: UIApplication.willResignActiveNotification)) { _ in
            // app move to background
        }
        .onReceive(NotificationCenter.default.publisher(for: UIApplication.willEnterForegroundNotification)) { _ in
            // app come to foregound, so refersh list and connect to bass service
            self.chats = BixDb.instance.getChats(_phone: self.contact.phone)
            
            //fetch()
            connect()
        }
        .sheet(isPresented: $requestSelfie) {
            SelfieCallView(contact: self.$contact)
        }
    }
    
    func connect() {
        let onMessage:(Message) -> () = { msg in
            print("got message uid in chat:" + msg.uid)
            
            // get contact
            var sender = BixDb.instance.getContact(_phone: msg.sender)
            if sender != nil {
                // contact should be here
                if self.currentContact == nil {
                    // this means not in chat
                    // increase unread count by one
                    sender!.unreadCount = sender!.unreadCount + 1
                    _ = BixDb.instance.updateUnreadCount(_contact: sender!.phone, count: 1, reset: false)
                    
                    // show notificaiotn here
                    let notification = BassaNotification(id: msg.uid, title: msg.sender, body: "New message received")
                    BassaNotificationManager.instance.postNotification(notification: notification)
                }
                
                // save chat
                let chat = Chat(uid: msg.uid, body: msg.body, timestamp:DateUtil.instance.getTimestamp(), contact: sender!, myChat: false)
                saveChat(chat: chat)
            }
        }
    
        DispatchQueue.global(qos: .userInitiated).async {
            var connect = Connect()
            connect.uid = self.prefsPhone + String(DateUtil.instance.getTimestamp())
            connect.sender = self.prefsPhone
            
            MessangerComm.instance.stremMessage(connect: connect, onMessage: onMessage)
        }
    }
    
    func fetch() {
        let onFetchResponse:(FetchResponse?) -> () = { resp in
            if resp != nil {
                print(resp!.messages)
                
                handleFetchResponse(resp: resp!)
            }
        }
        
        DispatchQueue.global(qos: .userInitiated).async {
            var req = FetchRequest()
            req.uid = self.prefsPhone + String(DateUtil.instance.getTimestamp())
            req.sender = self.prefsPhone
            
            MessangerComm.instance.fetchMessages(req: req, onFetchResponse: onFetchResponse)
        }
    }
    
    func handleFetchResponse(resp: FetchResponse) {
        // sort
        let msgs = resp.messages
        //msgs.sort(by: {$0.timestamp > $1.timestamp})
        
        print(msgs)
        
        for msg in msgs {
            if (msg.type == "MESSAGE") {
                // increase unread count
                var sender = BixDb.instance.getContact(_phone: msg.sender)
                if sender != nil {
                    // contact should be here
                    if sender?.phone != self.contact.phone {
                        // not current contact
                        sender!.unreadCount = sender!.unreadCount + 1
                        _ = BixDb.instance.updateUnreadCount(_contact: sender!.phone, count: 1, reset: false)
                        
                        // show notificaiotn here
                        let notification = BassaNotification(id: msg.uid, title: msg.sender, body: "New message received")
                        BassaNotificationManager.instance.postNotification(notification: notification)
                    }
                    
                    // save chat
                    let chat = Chat(uid: msg.uid, body: msg.body, timestamp:DateUtil.instance.getTimestamp(), contact: sender!, myChat: false)
                    saveChat(chat: chat)
                }
            } else if (msg.type == "FRIEND_REQUEST") {
                var sender = BixDb.instance.getContact(_phone: msg.sender)
                if sender == nil {
                    // no existing contact
                    // create contact
                    let pc = PhoneBookUtil.instance.getContact(phone: msg.sender)
                    let name = pc == nil ? msg.sender: pc!.firstName + " " + pc!.lastName
                    sender = Contact(email: name, phone: msg.sender, myRequest: false, isAccepted: false)
                    _ = BixDb.instance.createContact(contact: sender!)
                }
            } else if (msg.type == "ACCEPT_REQUEST") {
                var sender = BixDb.instance.getContact(_phone: msg.sender)
                if sender != nil {
                    // accept reqiest
                    let pc = PhoneBookUtil.instance.getContact(phone: msg.sender)
                    let name = pc == nil ? msg.sender: pc!.firstName + " " + pc!.lastName
                    sender = Contact(email: name, phone: msg.sender, myRequest: false, isAccepted: false)
                    _ = BixDb.instance.markContactAccepted(_phone: msg.sender)
                    
                    // update session key
                    let sessionKey = CryptoUtil.instance.decryptRSA(payload: msg.body)!
                    _ = BixDb.instance.updateContactSessionKey(_phone: msg.sender, _seesionKey: sessionKey)
                }
            }
        }
    }
    
    func sendChat() {
        let onReply:(Reply) -> () = { name in
            print("got status " + name.status)
        }
        
        DispatchQueue.global(qos: .userInitiated).async {
            var msg = Message()
            let timestamp = DateUtil.instance.getTimestamp()
            let body = CryptoUtil.instance.encrypAES(key: self.contact.sessionKey, payload: self.chatBody)!
            msg.uid = self.prefsPhone + String(timestamp)
            msg.type = "MESSAGE"
            msg.body = body
            msg.receiver = self.contact.phone
            msg.sender = prefsPhone
            msg.timestamp = timestamp
            
            MessangerComm.instance.sendMessage(message: msg, onReply: onReply)
            
            self.contact.unreadCount = 0
            let chat = Chat(uid: msg.uid, body: body, timestamp:DateUtil.instance.getTimestamp(), contact: contact, myChat: true)
            saveChat(chat: chat)
            
            self.chatBody = ""
        }
    }
    
    func saveChat(chat: Chat) {
        DispatchQueue.main.async {
            if BixDb.instance.createChat(chat: chat) > 0 {
                // saved chat
                if chat.contact.phone == self.contact.phone {
                    self.chats.insert(chat, at: 0)
                }
            }
        }
    }
}

struct ChatRowView: View {
    @State
    var chat: Chat
    
    var background: some View {
        if (chat.myChat) {
            return Color.gray.opacity(0.25)
        } else {
            return Color.black.opacity(0.8)
        }
    }
    
    var body: some View {
        VStack {
            if chat.myChat {
                HStack{
                    Spacer()
                    Text(CryptoUtil.instance.decrypAES(key: chat.contact.sessionKey, payload: chat.body) ?? chat.body)
                        .padding(EdgeInsets(top: 10, leading: 20, bottom: 10, trailing: 20))
                        .background(background)
                        .foregroundColor(Color.black)
                        .cornerRadius(24)
                        .frame(alignment: .leading)
                }
                HStack{
                    Spacer()
                    Text(DateUtil.instance.toReadleTime(timestamp: chat.timestamp))
                        .font(.subheadline)
                        .foregroundColor(Color.gray)
                        .frame(alignment: .leading)
                    Image("tick_disabled")
                        .resizable()
                        .frame(width: 12, height: 12)
                }
            } else {
                HStack{
                    Text(CryptoUtil.instance.decrypAES(key: chat.contact.sessionKey, payload: chat.body) ?? chat.body)
                        .padding(EdgeInsets(top: 10, leading: 20, bottom: 10, trailing: 20))
                        .background(background)
                        .foregroundColor(Color.white)
                        .cornerRadius(24)
                        .frame(alignment: .trailing)
                    Spacer()
                }
                HStack{
                    Text(DateUtil.instance.toReadleTime(timestamp: chat.timestamp))
                        .font(.subheadline)
                        .foregroundColor(Color.gray)
                        .frame(alignment: .trailing)
                    Spacer()
                }
            }
        }
        .frame(maxWidth: .infinity)
        .padding(EdgeInsets(top: 3, leading: 0, bottom: 3, trailing: 0))
    }
}

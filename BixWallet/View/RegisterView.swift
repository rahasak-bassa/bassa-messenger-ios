import SwiftUI
import ToastUI


struct RegisterView: View {
    
    @State var name: String = ""
    @State var email: String = ""
    @State var phone: String = ""
    @State var password: String = ""
    @State var confirmPassword: String = ""
    
    @State private var onProgress: Bool = false
    @State private var onSuccess: Bool = false
    @State private var onError: Bool = false
    @State private var regSuccess: Bool = false
    @State private var errorMsg: String = ""
    
    @EnvironmentObject var navigationObserver: NavigationObserver
    
    @Environment(\.presentationMode) var presentationMode
    
    var body: some View {
        NavigationView {
            GeometryReader { geometry in
                ScrollView(.vertical) {
                    VStack{
                        TextField("Name", text: $name)
                            .keyboardType(.default)
                            .autocapitalization(.none)
                            .disableAutocorrection(true)
                            .padding(.bottom, 5)
                            .background(Color.white)
                            .overlay(
                                Rectangle()
                                    .frame(height: 1)
                                    .foregroundColor(Color.gray.opacity(0.2)),
                                alignment: .bottom
                            )
                            .padding(.bottom, 40)
                        
                        TextField("Phone", text: $phone)
                            .keyboardType(.phonePad)
                            .autocapitalization(.none)
                            .disableAutocorrection(true)
                            .padding(.bottom, 5)
                            .background(Color.white)
                            .overlay(
                                Rectangle()
                                    .frame(height: 1)
                                    .foregroundColor(Color.gray.opacity(0.2)),
                                alignment: .bottom
                            )
                            .padding(.bottom, 40)
                        
                        TextField("Email", text: $email)
                            .keyboardType(.emailAddress)
                            .autocapitalization(.none)
                            .disableAutocorrection(true)
                            .padding(.bottom, 5)
                            .background(Color.white)
                            .overlay(
                                Rectangle()
                                    .frame(height: 1)
                                    .foregroundColor(Color.gray.opacity(0.2)),
                                alignment: .bottom
                            )
                            .padding(.bottom, 40)
                        
                        SecureField("PIN", text: $password)
                            .keyboardType(.numberPad)
                            .autocapitalization(.none)
                            .disableAutocorrection(true)
                            .padding(.bottom, 5)
                            .background(Color.white)
                            .overlay(
                                Rectangle()
                                    .frame(height: 1)
                                    .foregroundColor(Color.gray.opacity(0.2)),
                                alignment: .bottom
                            )
                            .padding(.bottom, 40)
                        
                        SecureField("Confirm PIN", text: $confirmPassword)
                            .keyboardType(.numberPad)
                            .autocapitalization(.none)
                            .disableAutocorrection(true)
                            .padding(.bottom, 5)
                            .background(Color.white)
                            .overlay(
                                Rectangle()
                                    .frame(height: 1)
                                    .foregroundColor(Color.gray.opacity(0.2)),
                                alignment: .bottom
                            )
                            .padding(.bottom, 50)
                        
                        Button(action: {
                            UIApplication.shared.windows.forEach { $0.endEditing(true)}
                            //doReg()
                            bassaRegister()
                        }){
                            Text("Register")
                                .font(.headline)
                                .frame(maxWidth: .infinity)
                                .padding()
                                .foregroundColor(.gray)
                                .overlay(
                                    RoundedRectangle(cornerRadius: 40)
                                        .stroke(Color.black.opacity(0.8), lineWidth: 1)
                                )
                                .padding(.bottom, 10)
                        }
                        
                        Button(action: {
                            self.navigationObserver.navigation = "login"
                            //getPartnerCert()
                        }){
                            Text("Already have an account? Log in")
                                .font(.subheadline)
                                .foregroundColor(.gray)
                                .padding(.bottom, 5)
                                .overlay(
                                    Rectangle()
                                        .frame(height: 1)
                                        .foregroundColor(Color.gray.opacity(0.2)),
                                    alignment: .bottom
                                )
                        }
                    }
                    .toast(isPresented: $onProgress) {
                        // finish progress
                        if self.regSuccess {
                            self.navigationObserver.navigation = "home"
                        } else {
                            self.onError = true
                        }
                    } content: {
                        ToastView()
                            .toastViewStyle(IndefiniteProgressToastViewStyle())
                    }
                    .toast(isPresented: $onSuccess, dismissAfter: 2.0) {
                        // finish dialog
                        self.navigationObserver.navigation = "home"
                    } content: {
                        ToastView("Registration done")
                            .toastViewStyle(SuccessToastViewStyle())
                    }
                    .toast(isPresented: $onError, dismissAfter: 2.0) {
                        // finish dialog
                    } content: {
                        ToastView(self.errorMsg)
                            .toastViewStyle(ErrorToastViewStyle())
                    }
                    .padding(EdgeInsets(top: 10, leading: 40, bottom: 10, trailing: 40))
                    .frame(width: geometry.size.width)
                    .frame(minHeight: geometry.size.height)
                    .navigationBarTitle("Register")
                }
                .onTapGesture {
                    UIApplication.shared.windows.forEach { $0.endEditing(true)}
                }
            }
        }
    }
    
    func doReg() {
        DispatchQueue.global(qos: .userInitiated).async {
            let status = CryptoUtil.instance.initKeys()
            if !status {
                self.regSuccess = false
            } else {
                let pubKey = PreferenceUtil.instance.get(key: PreferenceUtil.PUBLIC_KEY)
                print(pubKey)
                
                let bixId = BixUtil.instance.getBixId(digits: 10)
                print(bixId)
                
                let bixIdentity = BixUtil.instance.getBixIdentity(bixId: bixId, email: self.email, mobileNo: self.phone, ip: "0.0.0.0")
                print(bixIdentity)
                
                let bixReq = BixUtil.instance.getBixCertificateRequest(bixIdentity: bixIdentity, publicKey: pubKey)
                print(bixIdentity)
                
                let res = BixComm.instance.postMsg(bixReq: bixReq)
                if res != nil {
                    //self.navigationObserver.navigation = "home"
                }
            }
        }
    }
    
    func getPartnerCert() {
        DispatchQueue.global(qos: .userInitiated).async {
            let status = CryptoUtil.instance.initKeys()
            if !status {
                self.regSuccess = false
            } else {
                let bixReq = BixUtil.instance.getPartnerRegisterRequest(requester: self.email, partner: "12223334444")
                print(bixReq)
                
                let res = BixComm.instance.postMsg(bixReq: bixReq)
                if res != nil {
                    //self.navigationObserver.navigation = "home"
                }
            }
        }
    }
    
    func bassaRegister() {
        if self.email == "" || self.phone == "" || self.password == "" || self.confirmPassword == "" {
            self.onError = true
            self.errorMsg = "Empty fields"
            return
        }
        
        if password != confirmPassword {
            self.onError = true
            self.errorMsg = "Invalid password"
            return
        }
        
        let fPhone = PhoneNumberUtil.instance.internationalize(phone: self.phone)
        if fPhone == nil {
            self.onError = true
            self.errorMsg = "Invalid phone"
            return
        }
        phone = fPhone!.replacingOccurrences(of: " ", with: "")
        
        let onComplete:(Reply) -> () = { reply in
            print("got status " + reply.status)
            
            if reply.status == "OK" {
                // save prefs
                PreferenceUtil.instance.put(key: PreferenceUtil.ACCOUNT_STATUS, value: "DONE")
                PreferenceUtil.instance.put(key: PreferenceUtil.EMAIL, value: self.email)
                PreferenceUtil.instance.put(key: PreferenceUtil.PHONE, value: self.phone)
                PreferenceUtil.instance.put(key: PreferenceUtil.PASSWORD, value: self.password)
                
                // clear fields
                self.name = ""
                self.email = ""
                self.phone = ""
                self.password = ""
                self.confirmPassword = ""
                
                // success
                self.onProgress = false
                self.regSuccess = true
            } else {
                // display error
                self.onProgress = false
                self.regSuccess = false
            }
        }
        
        DispatchQueue.global(qos: .userInitiated).async {
            self.onProgress = true
            let status = CryptoUtil.instance.initKeys()
            if !status {
                self.regSuccess = false
                return
            }
            
            print(PreferenceUtil.instance.get(key: PreferenceUtil.PUBLIC_KEY))
            var dis = CryptoUtil.instance.sign(payload: "rahasak")
            print(dis)
            
            var device = Device()
            device.uid = phone + String(DateUtil.instance.getTimestamp())
            device.id = phone
            device.type = "apple"
            device.token = PreferenceUtil.instance.get(key: PreferenceUtil.FCM_TOKEN)
            device.pubkey = PreferenceUtil.instance.get(key: PreferenceUtil.PUBLIC_KEY)
            
            MessangerComm.instance.registerDevice(device: device, onReply: onComplete)
        }
    }
}

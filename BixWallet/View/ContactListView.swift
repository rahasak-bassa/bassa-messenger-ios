import SwiftUI

struct Contact: Identifiable, Hashable {
    let id = UUID()
    var email: String
    var phone: String
    var pubkey: String = ""
    var sessionKey: String = ""
    var unreadCount = 0
    var thumbnail: Data?
    var image: Data?
    var myRequest: Bool = false
    var isAccepted: Bool = false
    var isGroup: Bool = false
}

enum ActiveSheet: Identifiable {
    case contacts, groups
    
    var id: Int {
        hashValue
    }
}

struct ContactListView: View {
    @State var presentSheet = false
    @State var gotoAddContact = false
    @State var gotoNewGroup = false
    
    @State var activeSheet: ActiveSheet?
    @State var contacts: [Contact] = []
    @State var isExpanded = false
    
    var body: some View {
        ZStack {
            ScrollView(.vertical, showsIndicators: false) {
                LazyVStack {
                    ForEach(self.contacts){contact in
                        NavigationLink(destination: ContactDetailView(contact: contact)) {
                            ContactRowView(contact: contact)
                        }
                    }
                }
            }
            VStack {
                Spacer()
                HStack {
                    Spacer()
                    
                    ExpandableButtonPanel(
                        primaryItem: ExpandableButtonItem(label: "+"), // No action
                        secondaryItems: [
                            ExpandableButtonItem(label: "@") {
                                self.isExpanded = false
                                
                                activeSheet = .contacts
                            },
                            ExpandableButtonItem(label: "&") {
                                self.isExpanded = false
                                
                                activeSheet = .groups
                            }
                        ],
                        isExpanded: self.$isExpanded
                    )
                    
                    //                    Button(action: {
                    //                        self.gotoAddContact = true
                    //                    }, label: {
                    //                        Text("+")
                    //                            .font(.system(.largeTitle))
                    //                            .frame(width: 50, height: 50)
                    //                            .foregroundColor(Color.white)
                    //                            .background(Color.black)
                    //                            .clipShape(Circle())
                    //                            .padding(EdgeInsets(top: 0, leading: 0, bottom: 5, trailing: 5))
                    //                    })
                }
            }
            // empty view
            if self.contacts.count == 0 {
                VStack(alignment: .center) {
                    Spacer()
                    Image("ghost2")
                        .resizable()
                        .frame(width: 100, height: 100)
                    Text("No contacts found. Please click on (+) sign to add new contact.")
                        .font(.subheadline)
                        .foregroundColor(Color.gray)
                        .multilineTextAlignment(.center)
                        .padding(EdgeInsets(top: 5, leading: 40, bottom: 0, trailing: 40))
                    Spacer()
                }
            }
        }
        .onAppear {
            self.contacts = BixDb.instance.getContacts()
        }
        .sheet(item: $activeSheet) { item in
            switch item {
            case .contacts:
                GroupView(contacts: $contacts)
            case .groups:
                PhoneBookView(bassaContacts: $contacts)
            }
        }
    }
}

struct ContactRowView: View {
    @State
    var contact: Contact
    
    var body: some View {
        VStack {
            HStack {
                Image(uiImage: UIImage(data: contact.thumbnail ?? Data()) ?? UIImage(imageLiteralResourceName: "default_user_icon"))
                    .resizable()
                    .frame(width: 40, height: 40)
                    .clipShape(Circle())
                    .overlay(Circle().stroke(Color.gray.opacity(0.2), lineWidth: 1))
                
                VStack(alignment: .leading) {
                    HStack {
                        Text(contact.email)
                            .foregroundColor(Color.black)
                        Spacer()
                    }
                    .padding(.bottom, 7)
                    
                    HStack {
                        Text(contact.phone)
                            .font(.subheadline)
                            .foregroundColor(Color.gray)
                        Spacer()
                    }
                    
                    if (!contact.isAccepted) {
                        Text(contact.myRequest ? "Sent friend request" : "New friend request")
                            .font(.subheadline)
                            .padding(EdgeInsets(top: 7, leading: 15, bottom: 7, trailing: 15))
                            .background(Color.black.opacity(0.8))
                            .foregroundColor(Color.white)
                            .cornerRadius(24)
                            .frame(alignment: .trailing)
                            .padding(.top, 7)
                    }
                }
            }
            Divider()
        }
        .padding(EdgeInsets(top: 15, leading: 15, bottom: 15, trailing: 15))
    }
}

import SwiftUI


struct SplashView: View {
    
    @EnvironmentObject var navigationObserver: NavigationObserver
    
    var body: some View {
        VStack {
            if self.navigationObserver.navigation == "home" {
                HomeView()
            } else if self.navigationObserver.navigation == "login" {
                LoginView()
            } else if self.navigationObserver.navigation == "register" {
                RegisterView()
            } else if self.navigationObserver.navigation == "capture" {
                SelfieSplashView()
            } else {
                BixView()
            }
        }
        .onAppear {
            DispatchQueue.main.asyncAfter(deadline: .now() + 2.5) {
                withAnimation {
                    if PreferenceUtil.instance.get(key: PreferenceUtil.LOGIN_STATE) == "LOGIN" {
                        self.navigationObserver.navigation = "home"
                    } else if ImageUtil.instance.readImg(forKey: ImageUtil.PROFILE_IMAGE) == nil {
                        self.navigationObserver.navigation = "login"
                    } else {
                        self.navigationObserver.navigation = "login"
                    }
                }
            }
        }
        .accentColor(.black)
    }
}

struct BixView: View {
    var body: some View {
        VStack {
            Spacer()
            Image("bixo")
                .resizable()
                .frame(width: 120, height: 120)
                .clipShape(Circle())
            Spacer()
            
            Text("Enabled by the BIX Ledger")
                .font(.subheadline)
                //.fontWeight(.medium)
                .foregroundColor(.gray)
                //                .padding(.bottom, 5)
                //                .overlay(
                //                    Rectangle()
                //                        .frame(height: 1)
                //                        .foregroundColor(Color.gray.opacity(0.2)),
                //                    alignment: .bottom
                //                )
                .padding(.bottom, 20)
        }
        .frame(minWidth: 0,
               maxWidth: .infinity,
               minHeight: 0,
               maxHeight: .infinity,
               alignment: .center
        )
    }
}


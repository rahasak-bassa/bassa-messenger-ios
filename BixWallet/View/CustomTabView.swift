import SwiftUI

struct ContentTabView: View {
    var body: some View {
        NavigationView{
            Home()
                //.navigationTitle("Helo")
                .navigationBarTitleDisplayMode(.inline)
        }
    }
}

// tab items
var tabItems = ["Messages", "Contacts", "Preference"]

struct Home : View {
    @State var selected = "Messages"
    @State var centerX : CGFloat = 0
    
    init() {
        UITabBar.appearance().isHidden = true
    }
    
    func tabImage(selected: String) -> String {
        if selected == "Messages" {
            return "message"
        } else if selected == "Contacts" {
            return "person"
        } else if selected == "Preference" {
            return "gear"
        } else {
            return ""
        }
    }
    
    var body: some View{
        NavigationView {
            VStack(spacing: 0){
                TabView(selection: $selected){
                    RecentChatListView()
                        .tag(tabItems[0])
                    //.ignoresSafeArea(.all, edges: .top)
                    
                    ContactListView()
                        .tag(tabItems[1])
                    //.ignoresSafeArea(.all, edges: .top)
                    
                    ContactListView()
                        .tag(tabItems[2])
                    //.ignoresSafeArea(.all, edges: .top)
                }
                
                // custom tabbar
                HStack(spacing: 0){
                    ForEach(tabItems,id: \.self){value in
                        GeometryReader{reader in
                            TabBarButton(selected: $selected, value: value, image: tabImage(selected: value), centerX: $centerX, rect: reader.frame(in: .global))
                                // setting first initial curve
                                //.onAppear(perform: {
                                //    if value == tabItems.first{
                                //        centerX = reader.frame(in: .global).midX + 130
                                //    }
                                //})
                        }
                        .frame(width: 70, height: 50)
                        if value != tabItems.last{Spacer(minLength: 0)}
                    }
                }
                .padding(.horizontal, 25)
                .padding(.top)
                // for smaller size iphone padding will be 15 and for norch phones nor paddding
                .padding(.bottom,UIApplication.shared.windows.first?.safeAreaInsets.bottom == 0 ? 15 : UIApplication.shared.windows.first?.safeAreaInsets.bottom)
                .background(Color.white.clipShape(AnimatedShape(centerX: centerX)))
                .shadow(color: Color.black.opacity(0.1), radius: 5, x: 0, y: -5)
                .padding(.top, -15)
                .ignoresSafeArea(.all, edges: .horizontal)
            }
            .ignoresSafeArea(.all, edges: .bottom)
            .navigationTitle(self.selected)
        }
    }
}

struct TabBarButton : View {
    @Binding var selected : String
    var value: String
    var image: String
    @Binding var centerX : CGFloat
    var rect : CGRect
    
    var body: some View{
        Button(action: {
            withAnimation(.spring()){
                selected = value
                centerX = rect.midX
            }
        }, label: {
            VStack{
                Image(systemName: image)
                    .resizable()
                    .renderingMode(.template)
                    .frame(width: 26, height: 26)
                    .foregroundColor(selected == value ? .black : .gray)
                
                Text(value)
                    .font(.caption)
                    .foregroundColor(.black)
                    .opacity(selected == value ? 1 : 0.5)
            }
            // deafult frame for reading mid x axis for curve
            .padding(.top)
            .frame(width: 70, height: 50)
            .offset(y: selected == value ? -15 : 0)
        })
    }
}

// custom curve shape
struct AnimatedShape: Shape {
    var centerX : CGFloat
    
    // animating path
    var animatableData: CGFloat{
        get{return centerX}
        set{centerX = newValue}
    }
    
    func path(in rect: CGRect) -> Path {
        return Path{path in
            path.move(to: CGPoint(x: 0, y: 15))
            path.addLine(to: CGPoint(x: 0, y: rect.height))
            path.addLine(to: CGPoint(x: rect.width, y: rect.height))
            path.addLine(to: CGPoint(x: rect.width, y: 15))
            
            // curve
            path.move(to: CGPoint(x: centerX - 35, y: 15))
            path.addQuadCurve(to: CGPoint(x: centerX + 35, y: 15), control: CGPoint(x: centerX, y: -30))
        }
    }
}

import SwiftUI
import ToastUI
import SSToastMessage

enum SheetState {
    case none
    case selfie
    case contacts
}

struct RecentChatListView: View {
    
    @State
    private var inputImage: UIImage?
    
    @State
    private var onProgress = false
    
    @State
    private var viewPreapred = false
    
    @State
    private var selfieCallMessage: Message?
    
    @State
    var selfieCallContact: Contact?
    
    @State
    var navigateChat = false
    
    @State
    private var onSheet = false
    
    @State
    var sheetState: SheetState = .selfie
    
    @State
    var selectedContact: Contact?
    
    @State
    var recentChats: [Chat] = []
    
    private let prefsPhone = PreferenceUtil.instance.get(key: PreferenceUtil.PHONE)
    
    var body: some View {
        ZStack {
            ScrollView(.vertical, showsIndicators: false) {
                LazyVStack {
                    ForEach(self.recentChats){chat in
                        NavigationLink(destination: ChatView(contact: chat.contact)) {
                            RecentChatRowView(chat: chat)
                        }
                    }
                }
            }
            
            // empty view
            if self.recentChats.count == 0 && self.viewPreapred {
                VStack(alignment: .center) {
                    Spacer()
                    Image("ghost2")
                        .resizable()
                        .frame(width: 100, height: 100)
                    Text("No messages found. Please goto contacts to send message.")
                        .font(.subheadline)
                        .foregroundColor(Color.gray)
                        .multilineTextAlignment(.center)
                        .padding(EdgeInsets(top: 5, leading: 40, bottom: 0, trailing: 40))
                    Spacer()
                }
            }
            
            if self.selectedContact != nil {
                NavigationLink(destination: ChatView(contact: selectedContact!), isActive: self.$navigateChat) {
                    EmptyView()
                }
            }
        }
        .toast(isPresented: $onProgress) {
        } content: {
            ToastView()
                .toastViewStyle(IndefiniteProgressToastViewStyle())
        }
        .onAppear(perform: {
            // load contacts in background
            if PhoneBookUtil.instance.contacts.count == 0 {
                // contacts not loaded, load contacts
                self.onProgress = true
                DispatchQueue.global(qos: .userInitiated).async {
                    PhoneBookUtil.instance.initContacts()
                    DispatchQueue.main.async {
                        self.recentChats = BixDb.instance.getRecentChats()
                        self.onProgress = false
                        self.viewPreapred = true
                    }
                }
            } else {
                self.recentChats = BixDb.instance.getRecentChats()
                self.viewPreapred = true
            }
            
            // subscribe to selfi call notification
            NotificationCenter.default.addObserver(forName: BassaNotificationManager.selfieCallNotification, object: nil, queue: .main) { notification in
                
                // get selfie call message from notification
                let msg = notification.userInfo!["MESSAGE"] as! Message
                self.selfieCallMessage = msg
                self.selfieCallContact = BixDb.instance.getContact(_phone: msg.sender)
                
                self.sheetState = .selfie
                self.onSheet = true
            }
            
            BassaNotificationManager.instance.setBadge(count: 0)
            
            // connect stream
            connect()
            fetch()
        })
        .onReceive(NotificationCenter.default.publisher(for: UIApplication.willResignActiveNotification)) { _ in
            // app move to background
        }
        .onReceive(NotificationCenter.default.publisher(for: UIApplication.willEnterForegroundNotification)) { _ in
            // app come to foregound, so refresh list
            self.recentChats = BixDb.instance.getRecentChats()
            
            // connect stream
            connect()
            fetch()
        }
        .sheet(isPresented: $onSheet, onDismiss: {
            self.onDismisSheet()
        }, content: {
            SelfieCameraView(image: self.$inputImage, selfieCallContact: self.$selfieCallContact)
        })
    }
    
    func connect() {
        let onMessage:(Message) -> () = { msg in
            print("got message uid in recent list:" + msg.uid)
            
            handleConnectResponse(msg: msg)
        }
        
        DispatchQueue.global(qos: .userInitiated).async {
            var connect = Connect()
            connect.uid = self.prefsPhone + String(DateUtil.instance.getTimestamp())
            connect.sender = self.prefsPhone
            
            MessangerComm.instance.stremMessage(connect: connect, onMessage: onMessage)
        }
    }
    
    func handleConnectResponse(msg: Message) {
        // get contact
        var sender = BixDb.instance.getContact(_phone: msg.sender)
        if sender != nil {
            // this means not in chat
            // increase unread count by one
            sender!.unreadCount = sender!.unreadCount + 1
            _ = BixDb.instance.updateUnreadCount(_contact: sender!.phone, count: 1, reset: false)
            
            // show notificaiotn here
            let notification = BassaNotification(id: msg.uid, title: msg.sender, body: "New message received")
            BassaNotificationManager.instance.postNotification(notification: notification)
            
            // save chat
            let chat = Chat(uid: msg.uid, body: msg.body, timestamp:DateUtil.instance.getTimestamp(), contact: sender!, myChat: false)
            if BixDb.instance.createChat(chat: chat) > 0 {
                // saved chat
                let index = self.recentChats.firstIndex(where: { $0.contact.phone == chat.contact.phone })
                if index == nil {
                    self.recentChats.append(chat)
                } else {
                    self.recentChats[index!] = chat
                }
            }
        }
    }
    
    func fetch() {
        let onFetchResponse:(FetchResponse?) -> () = { resp in
            if resp != nil {
                print(resp!.messages)
                
                handleFetchResponse(resp: resp!)
            }
        }
        
        DispatchQueue.global(qos: .userInitiated).async {
            var req = FetchRequest()
            req.uid = self.prefsPhone + String(DateUtil.instance.getTimestamp())
            req.sender = self.prefsPhone
            
            MessangerComm.instance.fetchMessages(req: req, onFetchResponse: onFetchResponse)
        }
    }
    
    func handleFetchResponse(resp: FetchResponse) {
        // sort
        let msgs = resp.messages
        //msgs.sort(by: {$0.timestamp > $1.timestamp})
        
        for msg in msgs {
            if (msg.type == "MESSAGE") {
                // increase unread count
                var sender = BixDb.instance.getContact(_phone: msg.sender)
                sender!.unreadCount = sender!.unreadCount + 1
                _ = BixDb.instance.updateUnreadCount(_contact: sender!.phone, count: 1, reset: false)
                
                // save chat
                let chat = Chat(uid: msg.uid, body: msg.body, timestamp:DateUtil.instance.getTimestamp(), contact: sender!, myChat: false)
                if BixDb.instance.createChat(chat: chat) > 0 {
                    // saved chat
                    let index = self.recentChats.firstIndex(where: { $0.contact.phone == chat.contact.phone })
                    if index == nil {
                        self.recentChats.append(chat)
                    } else {
                        self.recentChats[index!] = chat
                    }
                }
            } else if (msg.type == "FRIEND_REQUEST") {
                var sender = BixDb.instance.getContact(_phone: msg.sender)
                if sender == nil {
                    // no existing contact
                    // create contact
                    let pc = PhoneBookUtil.instance.getContact(phone: msg.sender)
                    let name = pc == nil ? msg.sender: pc!.firstName + " " + pc!.lastName
                    sender = Contact(email: name, phone: msg.sender, myRequest: false, isAccepted: false)
                    _ = BixDb.instance.createContact(contact: sender!)
                }
            } else if (msg.type == "ACCEPT_REQUEST") {
                var sender = BixDb.instance.getContact(_phone: msg.sender)
                if sender != nil {
                    // accept reqiest
                    let pc = PhoneBookUtil.instance.getContact(phone: msg.sender)
                    let name = pc == nil ? msg.sender: pc!.firstName + " " + pc!.lastName
                    sender = Contact(email: name, phone: msg.sender, myRequest: false, isAccepted: false)
                    _ = BixDb.instance.markContactAccepted(_phone: msg.sender)
                    
                    // update session key
                    let sessionKey = CryptoUtil.instance.decryptRSA(payload: msg.body)!
                    _ = BixDb.instance.updateContactSessionKey(_phone: msg.sender, _seesionKey: sessionKey)
                }
            }
        }
    }
    
    func onDismisSheet() {
        if self.sheetState == .selfie {
            // send selfie back to selfie caller
            let onReply:(Reply) -> () = { reply in
                print("got status " + reply.status)
            }
            
            DispatchQueue.global(qos: .userInitiated).async {
                // compress and rotate image
                let rI = inputImage?.resized(withPercentage: 0.2)
                let fI = rI!.flip()
                
                // send message
                var msg = Message()
                let timestamp = DateUtil.instance.getTimestamp()
                msg.uid = self.prefsPhone + String(timestamp)
                msg.type = "SELFIE"
                msg.body = ImageUtil.instance.encodeToBase64(image: fI!)
                msg.sender = prefsPhone
                msg.receiver = self.selfieCallContact!.phone
                msg.timestamp = timestamp
                
                MessangerComm.instance.sendMessage(message: msg, onReply: onReply)
            }
        } else {
            // navigate chat
            self.navigateChat = true
        }
        
        // reset sheet
        self.selectedContact = nil
        self.onSheet = false
    }
    
    func saveChat(chat: Chat) {
        DispatchQueue.main.async {
            if BixDb.instance.createChat(chat: chat) > 0 {
                // saved chat
                let index = self.recentChats.firstIndex(where: { $0.contact.phone == chat.contact.phone })
                if index == nil {
                    self.recentChats.append(chat)
                } else {
                    self.recentChats[index!] = chat
                }
            }
        }
    }
}

struct RecentChatRowView: View {
    @State
    var chat: Chat
    
    var body: some View {
        VStack {
            HStack {
                Image(uiImage: UIImage(data: chat.contact.thumbnail ?? Data()) ?? UIImage(imageLiteralResourceName: "default_user_icon"))
                    .resizable()
                    .frame(width: 40, height: 40)
                    .clipShape(Circle())
                    .overlay(Circle().stroke(Color.gray.opacity(0.2), lineWidth: 1))
                
                VStack(alignment: .leading) {
                    HStack {
                        Text(chat.contact.email)
                            .foregroundColor(Color.black)
                        Spacer()
                        Text(DateUtil.instance.toReadleTime(timestamp: chat.timestamp))
                            .font(.subheadline)
                            .foregroundColor(Color.black)
                    }
                    .padding(.bottom, 7)
                    HStack {
                        Text(chat.body)
                            .lineLimit(1)
                            .font(.subheadline)
                            .foregroundColor(Color.gray)
                        Spacer()
                        
                        if (chat.contact.unreadCount > 0) {
                            VStack(alignment: .center) {
                                Text(String(chat.contact.unreadCount))
                                    .foregroundColor(Color.white)
                                    .font(.system(size: 11))
                            }
                            .frame(minWidth: 17, maxWidth: 17, minHeight: 17, maxHeight: 17)
                            .background(Color.black)
                            .clipShape(Circle())
                        }
                    }
                }
            }
            Divider()
        }
        .padding(EdgeInsets(top: 15, leading: 15, bottom: 15, trailing: 15))
    }
}


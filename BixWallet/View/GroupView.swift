import SwiftUI
import ToastUI

struct GroupView: View {
    @State var gotoAddContact = false
    @State var groupName: String = ""
    
    @State private var onError: Bool = false
    @State private var onSuccess: Bool = false
    @State private var errorMsg: String = ""
    
    @Binding var contacts: [Contact]
    @State var selections: [Contact] = []
    
    var body: some View {
        NavigationView {
            ZStack {
                VStack {
                    TextField("Group name", text: $groupName)
                        .keyboardType(.default)
                        .autocapitalization(.none)
                        .disableAutocorrection(true)
                        .padding(EdgeInsets(top: 0, leading: 10, bottom: 0, trailing: 10))
                        .background(Color.white)
                        .frame(height: 40)
                        .overlay(
                            RoundedRectangle(cornerRadius: 10)
                                .stroke(Color.gray.opacity(0.2), lineWidth: 1)
                        )
                        .padding(EdgeInsets(top: 10, leading: 15, bottom: 10, trailing: 15))
                    
                    ScrollView(.vertical, showsIndicators: false) {
                        LazyVStack {
                            ForEach(self.contacts){contact in
                                GroupRowView(contact: contact, isSelected: self.selections.contains(contact)) {
                                    if self.selections.contains(contact) {
                                        self.selections.removeAll(where: { $0 == contact })
                                    }
                                    else {
                                        self.selections.append(contact)
                                    }
                                }
                            }
                        }
                    }
                }
            }
            .navigationTitle("New group")
            .navigationBarItems(trailing:
                                    Button(action: {
                                        createGroup()
                                    }) {
                                        Text("Create")
                                    }
            )
            .onAppear {
                //self.contacts = BixDb.instance.getContacts(_isAccepted: true)
            }
            .toast(isPresented: $onError, dismissAfter: 2.0) {
                // finish dialog
            } content: {
                ToastView(self.errorMsg)
                    .toastViewStyle(ErrorToastViewStyle())
            }
        }
    }
    
    func createGroup() {
        // validate group name
        if self.groupName == "" {
            self.onError = true
            self.errorMsg = "Invalid name"
            return
        }
        
        // validate selection
        if self.selections.count == 0 {
            self.onError = true
            self.errorMsg = "Empty group"
            return
        }  
        
        // create group
        saveContact()
    }
    
    func saveContact() {
        let contact = Contact(email: self.groupName, phone: self.selections.map({$0.phone}).joined(separator: ";"),
                              pubkey: "", isAccepted: true, isGroup: true)
        
        DispatchQueue.main.async {
            // create contact in db
            if BixDb.instance.createContact(contact: contact) > 0 {
                // todo handle this
            } else {
                //self.onError = true
            }
        }
    }
}

struct GroupRowView: View {
    @State
    var contact: Contact
    var isSelected: Bool
    var action: () -> Void
    
    var body: some View {
        Button(action: self.action) {
            VStack {
                HStack {
                    Image(uiImage: UIImage(data: contact.thumbnail ?? Data()) ?? UIImage(imageLiteralResourceName: "default_user_icon"))
                        .resizable()
                        .frame(width: 40, height: 40)
                        .clipShape(Circle())
                        .overlay(Circle().stroke(Color.gray.opacity(0.2), lineWidth: 1))
                    
                    VStack(alignment: .leading) {
                        HStack {
                            Text(contact.email)
                                .foregroundColor(Color.black)
                            Spacer()
                        }
                        .padding(.bottom, 7)
                        
                        HStack {
                            Text(contact.phone)
                                .font(.subheadline)
                                .foregroundColor(Color.gray)
                            Spacer()
                        }
                    }
                    
                    if self.isSelected {
                        Spacer()
                        Image(systemName: "checkmark")
                    }
                }
                Divider()
            }
            .padding(EdgeInsets(top: 15, leading: 15, bottom: 15, trailing: 15))
        }
    }
}

struct ExpandableButtonItem: Identifiable {
    let id = UUID()
    let label: String
    private(set) var action: (() -> Void)? = nil
}

struct ExpandableButtonPanel: View {
    
    let primaryItem: ExpandableButtonItem
    let secondaryItems: [ExpandableButtonItem]
    
    private let noop: () -> Void = {}
    private let size: CGFloat = 50
    private var cornerRadius: CGFloat {
        get { size / 2 }
    }
    private let shadowColor = Color.black.opacity(0.4)
    private let shadowPosition: (x: CGFloat, y: CGFloat) = (x: 2, y: 2)
    private let shadowRadius: CGFloat = 3
    
    @Binding var isExpanded: Bool
    
    var body: some View {
        VStack {
            ForEach(secondaryItems) { item in
                Button(item.label, action: item.action ?? self.noop)
                    .frame(
                        width: self.isExpanded ? self.size : 0,
                        height: self.isExpanded ? self.size : 0)
            }
            
            Button(primaryItem.label, action: {
                withAnimation {
                    self.isExpanded.toggle()
                }
                self.primaryItem.action?()
            })
            .frame(width: size, height: size)
        }
        .background(Color(UIColor.black))
        .foregroundColor(.white)
        .cornerRadius(cornerRadius)
        .font(.title)
        .shadow(
            color: shadowColor,
            radius: shadowRadius,
            x: shadowPosition.x,
            y: shadowPosition.y
        )
    }
}

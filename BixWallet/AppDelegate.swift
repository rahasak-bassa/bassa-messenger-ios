import Firebase
import FirebaseMessaging
import UserNotifications
import Foundation
import UIKit

//class AppDelegate: NSObject {
//
//    let gcmMessageIDKey = "gcm.message_id"
//
//    private func setupFirebase(application: UIApplication) {
//        FirebaseApp.configure()
//
//        if #available(iOS 10.0, *) {
//            // For iOS 10 display notification (sent via APNS)
//            UNUserNotificationCenter.current().delegate = self
//
//            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
//            UNUserNotificationCenter.current().requestAuthorization(
//                options: authOptions,
//                completionHandler: {_, _ in })
//        } else {
//            let settings: UIUserNotificationSettings = UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
//            application.registerUserNotificationSettings(settings)
//        }
//
//        Messaging.messaging().delegate = self
//        application.registerForRemoteNotifications()
//    }
//
//}

class AppDelegate:NSObject, UIApplicationDelegate {
    let gcmMessagePayloadKey = "gcm.notification.message"
    
    func application(_ application: UIApplication,
                     didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]? = nil) -> Bool {
        // configure firebase
        FirebaseApp.configure()
        
        if #available(iOS 10.0, *) {
            // for iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
        } else {
            let settings: UIUserNotificationSettings = UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        
        Messaging.messaging().delegate = self
        application.registerForRemoteNotifications()
        
        return true
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
        // if you are receiving a notification message while your app is in the background,
        // this callback will not be fired till the user taps on the notification launching the application.
        // TODO: Handle data of notification
        
        // with swizzling disabled you must let Messaging know about the message, for Analytics
        Messaging.messaging().appDidReceiveMessage(userInfo)
        
        print("got message in UIApplicationDelegate1 - \(userInfo)")
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        // with swizzling disabled you must let Messaging know about the message, for Analytics
        Messaging.messaging().appDidReceiveMessage(userInfo)
        
        print("got message in UIApplicationDelegate2 - \(userInfo)")
        
        completionHandler(UIBackgroundFetchResult.newData)
    }
}

extension AppDelegate: MessagingDelegate {
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String?) {
        print("got firebase token: \(fcmToken!)")
        
        // set updated token in prefs
        if PreferenceUtil.instance.get(key: PreferenceUtil.FCM_TOKEN) != fcmToken! {
            PreferenceUtil.instance.put(key: PreferenceUtil.FCM_TOKEN, value: fcmToken!)
            PreferenceUtil.instance.put(key: PreferenceUtil.UPDATE_FCM_TOKEN, value: "yes")
        }
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        Messaging.messaging().apnsToken = deviceToken
    }
}

@available(iOS 10, *)
extension AppDelegate: UNUserNotificationCenterDelegate {
    // called if app is running in foreground
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        let userInfo = notification.request.content.userInfo
        
        print("got message in UNUserNotificationCenterDelegate1 - \(userInfo)")
        
        // with swizzling disabled you must let Messaging know about the message, for Analytics
        Messaging.messaging().appDidReceiveMessage(userInfo)
        
        if let messagePayload = userInfo[gcmMessagePayloadKey] {
            print("notification payload: \(messagePayload)")
            BassaNotificationManager.instance.handleBassaMessage(jsonData: messagePayload as! String)
        }
        
        completionHandler([.banner, .badge, .sound])
    }
    
    // called when user interacts with notification (app not running in foreground)
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void) {
        let userInfo = response.notification.request.content.userInfo
        
        print("got message in UNUserNotificationCenterDelegate2 - \(userInfo)")
        
        if let messagePayload = userInfo[gcmMessagePayloadKey] {
            print("notification payload: \(messagePayload)")
            BassaNotificationManager.instance.handleBassaMessage(jsonData: messagePayload as! String)
        }
        
        Messaging.messaging().appDidReceiveMessage(userInfo)
        
        completionHandler()
    }
}

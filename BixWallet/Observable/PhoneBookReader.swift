import Foundation
import Contacts
import PhoneNumberKit


struct ContactInfo : Identifiable {
    var id = UUID()
    var firstName: String
    var lastName: String
    var phoneNumber: String?
    var thumbnail: Data?
    var image: Data?
}

class PhoneBookReader: ObservableObject {
    
    @Published var contacts: [ContactInfo] = []
    @Published var error: Error? = nil
    
    var phoneNumberKit = PhoneNumberKit()
    
    func fetchContacts() {
        let store = CNContactStore()
        store.requestAccess(for: .contacts) { (granted, error) in
            if let error = error {
                print("failed to request access", error)
                return
            }
            
            if granted {
                let keys = [CNContactGivenNameKey, CNContactFamilyNameKey, CNContactPhoneNumbersKey, CNContactThumbnailImageDataKey]
                let request = CNContactFetchRequest(keysToFetch: keys as [CNKeyDescriptor])
                
                request.sortOrder = .givenName
                
                do {
                    var contactsArray = [ContactInfo]()
                    try store.enumerateContacts(with: request, usingBlock: { (contact, stopPointer) in
                        if (contact.phoneNumbers.first?.value.stringValue) != nil {
                            contactsArray.append(
                                ContactInfo(firstName: contact.givenName,
                                            lastName: contact.familyName,
                                            phoneNumber: contact.phoneNumbers.first?.value.stringValue)
                            )
                        }
                    })
                    self.contacts = contactsArray
                } catch let error {
                    print("Failed to enumerate contact", error)
                }
            } else {
                print("access denied")
            }
        }
    }
    
    func requestAccess() {
        let store = CNContactStore()
        switch CNContactStore.authorizationStatus(for: .contacts) {
        case .authorized:
            self.fetchingContacts()
        case .denied:
            store.requestAccess(for: .contacts) { granted, error in
                if granted {
                    self.fetchingContacts()
                }
            }
        case .restricted, .notDetermined:
            store.requestAccess(for: .contacts) { granted, error in
                if granted {
                    self.fetchingContacts()
                }
            }
        @unknown default:
            print("error")
        }
    }
    
    func fetchingContacts() {
        var arr = [ContactInfo]()
        let keys = [CNContactGivenNameKey, CNContactFamilyNameKey, CNContactPhoneNumbersKey, CNContactThumbnailImageDataKey]
        let request = CNContactFetchRequest(keysToFetch: keys as [CNKeyDescriptor])
        do {
            try CNContactStore().enumerateContacts(with: request, usingBlock: { (contact, stopPointer) in
                if (contact.phoneNumbers.first?.value.stringValue) != nil {
                    let phone = PhoneNumberUtil.instance.internationalize(phone: (contact.phoneNumbers.first?.value.stringValue)!)
                    arr.append(
                        ContactInfo(firstName: contact.givenName,
                                    lastName: contact.familyName,
                                    phoneNumber: phone,
                                    thumbnail: contact.thumbnailImageData
                        ))
                }
            })
        } catch let error {
            print("Failed", error)
        }
        arr = arr.sorted {
            $0.firstName < $1.firstName
        }
        
        self.contacts = arr
    }
    
}

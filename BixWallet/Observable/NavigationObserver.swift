import Foundation

class NavigationObserver: ObservableObject {
    @Published var navigation: String = ""
}

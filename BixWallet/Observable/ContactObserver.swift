import Combine
import Foundation

class ContactObserver: ObservableObject {
    @Published var contacts = [Contact]()
    
    init() {
        contacts = BixDb.instance.getContacts()
    }
    
    func initContacts() {
        contacts = BixDb.instance.getContacts()
    }
    
    func addContact(contact: Contact) {
        contacts.append(contact)
    }
    
    func removeContact(contact: Contact) {
        contacts.remove(at: 1)
    }
}

import Combine
import Foundation

class RecentChatObserver: ObservableObject {
    @Published var recentChats: [Chat] = []
    
    func initRecentChats() {
        recentChats = BixDb.instance.getRecentChats()
    }
    
    func addRecentChat(chat: Chat) {
        let index = self.recentChats.firstIndex(where: { $0.contact.phone == chat.contact.phone })
        if index == nil {
            recentChats.append(chat)
        } else {
            recentChats[index!] = chat
        }
    }
    
    func updateUnread(contact: Contact, count: Int) {
        let index = self.recentChats.firstIndex(where: { $0.contact.phone == contact.phone })
        if index != nil {
            var chat = recentChats[index!]
            chat.contact.unreadCount = count
            recentChats[index!] = chat
        }
    }
}



import Combine
import Foundation

class ChatObserver: ObservableObject {
    @Published var currentContact: Contact?
    @Published var chats = [Chat]()
    
    func initChats(phone: String) {
        chats = BixDb.instance.getChats(_phone: phone)
    }
    
    func addChat(chat: Chat) {
        chats.insert(chat, at: 0)
    }
    
    func setCurrentContact(contact: Contact?) {
        currentContact = contact
    }
}

import Foundation

class BixUtil {
    
    static let instance = BixUtil()
    
    func getBixId(digits:Int) -> String {
        var number = String()
        for _ in 1...digits {
           number += "\(Int.random(in: 1...9))"
        }
        return number
    }
    
    func getBixIdentity(bixId: String, email: String, mobileNo: String, ip: String) -> String {
        var createMap = [String: String]()
        createMap["bixID"] = bixId
        createMap["validated"] = "0"
        createMap["DN"] = "---------------------";
        createMap["countryName"] = ""
        createMap["state"] = ""
        createMap["localityName"] = ""
        createMap["organizationName"] = ""
        createMap["OU"] = ""
        createMap["commonName"] = email
        createMap["altNameURL"] = email
        createMap["altNameIP"] = ip
        createMap["PII"] = "---------------------"
        createMap["firstName"] = ""
        createMap["middleName"] = ""
        createMap["lastName"] = ""
        createMap["email"] = email
        createMap["mobileNumber"] = mobileNo
        createMap["address1"] = ""
        createMap["address2"] = ""
        createMap["city"] = ""
        createMap["DoB"] = ""
        createMap["gender"] = ""
        createMap["nationalIDNo"] = ""
        createMap["drivingLicenseState"] = ""
        createMap["drivingLicenseNo"] = ""
        createMap["nationality"] = ""
        createMap["passportNo"] = ""
        createMap["facebookPage"] = ""
        createMap["linkedInURL"] = ""
        createMap["skypeId"] = ""
        createMap["twiterAccount"] = ""
        
        let encoder = JSONEncoder()
        if let jsonData = try? encoder.encode(createMap) {
            if let jsonString = String(data: jsonData, encoding: .utf8) {
                print(jsonString)
                return jsonString
            }
        }
        
        return ""
    }
    
    func getBixCertificateRequest(bixIdentity: String, publicKey: String) -> String {
        let pemKey = "-----BEGIN PUBLIC KEY-----" + publicKey + "-----END PUBLIC KEY-----"
        let extendedPKCS10Request = bixIdentity + "<x>" + pemKey
        let bixCertificateRequestMessage = "pkcs10WalletPortalCertificateRequest<->" + extendedPKCS10Request + "<end>"

        return bixCertificateRequestMessage
    }
    
    func getPartnerRegisterRequest(requester: String, partner: String) -> String {
        let request = "registerpartner<->requesteremail=" + requester + ";partnerMobileNo=" + partner + ";source=mobile<end>";
                
        return request;
    }
    
}

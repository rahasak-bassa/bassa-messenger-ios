import Foundation
import SwiftGRPC
import SwiftUI

class MessangerComm {
    
    static let instance = MessangerComm()
    
    private var client: BassaServiceServiceClient
    
    init() {
        client = BassaServiceServiceClient.init(
            address: "198.252.98.138:9000",
            secure: false,
            arguments: [
                Channel.Argument.keepAliveTime(60000),
                Channel.Argument.keepAliveTimeout(10000),
                Channel.Argument.keepAlivePermitWithoutCalls(true)
            ]
        )
        client.timeout = 3600
    }
    
    func registerDevice(device: Device, onReply: @escaping (Reply) -> Void) {
        _ = try? client.registerDevice(device, completion: { (reply, result) in
            if result.statusCode == StatusCode.ok && reply != nil {
                onReply(reply!)
            } else {
                var r = Reply()
                r.status = "ERROR"
                onReply(r)
            }
        })
    }
    
    func findDevice(req: DeviceRequest, onDeviceReply: @escaping (Device?) -> Void) {
        _ = try? client.findDevice(req, completion: { (reply, result) in
            if result.statusCode == StatusCode.ok && reply != nil {
                onDeviceReply(reply!)
            } else {
                onDeviceReply(nil)
            }
        })
    }
    
    func fetchMessages(req: FetchRequest, onFetchResponse: @escaping (FetchResponse?) -> Void) {
        _ = try? client.fetchMessages(req, completion: { (reply, result) in
            if result.statusCode == StatusCode.ok && reply != nil {
                onFetchResponse(reply!)
            } else {
                onFetchResponse(nil)
            }
        })
    }
    
    func sendMessage(message: Message, onReply: @escaping (Reply) -> Void) {
        _ = try? client.sendMessage(message, completion: { (reply, result) in
            if result.statusCode == StatusCode.ok && reply != nil {
                if reply != nil {
                    onReply(reply!)
                } else {
                    var r = Reply()
                    r.status = "ERROR"
                    onReply(r)
                }
            }
        })
    }
    
    func sendAwa(_message: Message) {
        var message = Message()
        message.uid = _message.uid
        message.type = "AWA"
        message.sender = _message.receiver
        message.receiver = _message.sender
        message.digsig = ""
        
        _ = try? client.sendAwa(message, completion: { (reply, result) in
            if result.statusCode == StatusCode.ok && reply != nil {
                print("got reply for awa \(reply!)")
            }
        })
    }
    
    func stremMessage(connect: Connect, onMessage: @escaping (Message) -> Void) {
        do {
            var streaming = true
            
            let stream = try self.client.streamMessage(connect, completion: { err in
                print("false stream... \(err)")
                
                streaming = false
            })
            
            while streaming {
                if let message = try stream.receive() {
                    DispatchQueue.main.async {
                        // got message
                        print("got message into messanger comm uid:" + message.uid + " type:" + message.type)
                        
                        // TODO save chat
                        // TODO show notification if not current chat user
                        
                        // publish notifications for selfie and selfie_call message
                        if (message.type == "SELFIE_CALL") {
                            self.handleSelfieCall(msg: message)
                        } else if(message.type == "SELFIE") {
                            self.handleSelfie(msg: message)
                        } else if(message.type == "FRIEND_REQUEST") {
                            self.handleFriendRequest(msg: message)
                        } else if(message.type == "ACCEPT_REQUEST") {
                            self.handleAcceptRequest(msg: message)
                        } else if(message.type == "AWA") {
                            self.handleAwa(msg: message)
                        } else {
                            // send AWA first
                            //self.sendAwa(_message: message)
                            
                            onMessage(message)
                        }
                    }
                }
            }
            
            // end stream
            print("end stream...")
        } catch {
            // error stream
            print("error stream... \(error)")
        }
    }
    
    func handleSelfieCall(msg: Message) {
        BassaNotificationManager.instance
            .publishNotification(message: msg, notification: BassaNotificationManager.selfieCallNotification)
    }
    
    func handleSelfie(msg: Message) {
        BassaNotificationManager.instance
            .publishNotification(message: msg, notification: BassaNotificationManager.selfieNotification)
    }
    
    func handleFriendRequest(msg: Message) {
        var sender = BixDb.instance.getContact(_phone: msg.sender)
        if sender == nil {
            // no existing contact
            // create contact
            let pc = PhoneBookUtil.instance.getContact(phone: msg.sender)
            let name = pc == nil ? msg.sender: pc!.firstName + " " + pc!.lastName
            sender = Contact(email: name, phone: msg.sender, myRequest: false, isAccepted: false)
            _ = BixDb.instance.createContact(contact: sender!)
            
            // show notification
            let notification = BassaNotification(id: msg.uid, title: name, body: "New friend request received")
            BassaNotificationManager.instance.postNotification(notification: notification)
        }
    }
    
    func handleAcceptRequest(msg: Message) {
        var sender = BixDb.instance.getContact(_phone: msg.sender)
        if sender != nil {
            // accept request
            let pc = PhoneBookUtil.instance.getContact(phone: msg.sender)
            let name = pc == nil ? msg.sender: pc!.firstName + " " + pc!.lastName
            sender = Contact(email: name, phone: msg.sender, myRequest: false, isAccepted: false)
            _ = BixDb.instance.markContactAccepted(_phone: msg.sender)
            
            // update session key
            let sessionKey = CryptoUtil.instance.decryptRSA(payload: msg.body)!
            _ = BixDb.instance.updateContactSessionKey(_phone: msg.sender, _seesionKey: sessionKey)
            
            // show notification
            let notification = BassaNotification(id: msg.uid, title: name, body: "Friend request accepted")
            BassaNotificationManager.instance.postNotification(notification: notification)
        }
    }
    
    func handleAwa(msg: Message) {
        
    }
    
    func handleMessage(msg: Message) {
        BassaNotificationManager.instance
            .publishNotification(message: msg, notification: BassaNotificationManager.messageNotification)
    }
    
}

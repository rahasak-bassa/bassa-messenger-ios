import Foundation
import Contacts
import PhoneNumberKit

class PhoneBookUtil {
    
    static let instance = PhoneBookUtil()
    
    let store = CNContactStore()
    lazy var contacts: [ContactInfo] = []
    
    func initContacts() {
        var contacts = [ContactInfo]()
        let keys = [CNContactGivenNameKey, CNContactFamilyNameKey, CNContactPhoneNumbersKey, CNContactThumbnailImageDataKey, CNContactImageDataAvailableKey, CNContactImageDataKey]
        let request = CNContactFetchRequest(keysToFetch: keys as [CNKeyDescriptor])
        do {
            try store.enumerateContacts(with: request, usingBlock: { (contact, stopPointer) in
                if (contact.phoneNumbers.first?.value.stringValue) != nil {
                    let phone = PhoneNumberUtil.instance.internationalize(phone: (contact.phoneNumbers.first?.value.stringValue)!)
                    contacts.append(
                        ContactInfo(firstName: contact.givenName,
                                    lastName: contact.familyName,
                                    phoneNumber: phone,
                                    thumbnail: contact.thumbnailImageData,
                                    image: contact.imageDataAvailable ? contact.imageData : nil
                        )
                    )
                }
            })
        } catch let error {
            print("Failed", error)
        }
        
        contacts = contacts.sorted {
            $0.firstName < $1.firstName
        }
        
        self.contacts = contacts;
    }
    
    func getContacts() -> [ContactInfo] {
        return self.contacts
    }
    
    func getContact(phone: String) -> ContactInfo? {
        // init contacts first
//        if (contacts.count == 0) {
//            self.initContacts()
//        }
        
        // the fetch contact name
        if let i = contacts.firstIndex(where: {$0.phoneNumber != nil && $0.phoneNumber! == phone }) {
            return contacts[i]
        } else {
            return nil
        }
    }
    
}

import Foundation
import SQLite

class BixDb {
    static let instance = BixDb()
    
    private let db: Connection?
    
    // generic primay key
    private let pk = Expression<Int>("pk")
    
    // contact table
    private let contacts = Table("contacts")
    private let email = Expression<String>("email")
    private let phone = Expression<String>("phone")
    private let pubkey = Expression<String>("pubkey")
    private let sessionKey = Expression<String>("session_key")
    private let unreadCount = Expression<Int>("unread_count")
    private let myRequest = Expression<Bool>("my_request")
    private let isAccepted = Expression<Bool>("is_accepted")
    private let isGroup = Expression<Bool>("is_group")
    
    // chat table
    private let chats = Table("chats")
    private let id = Expression<String>("id")
    private let contact = Expression<String>("contact")
    private let body = Expression<String>("body")
    private let myChat = Expression<Bool>("my_chat")
    private let isViewed = Expression<Bool>("is_viewed")
    private let inOrder = Expression<Bool>("in_order")
    private let timestamp = Expression<Int64>("timestamp")
    
    // recents chats
    private let recentChats = Table("recent_chats")
    
    private init() {
        let path = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first!
        do {
            db = try Connection("\(path)/Bassa.sqlite3")
        } catch {
            db = nil
            print ("Unable to open database")
        }
        
        createTableContact()
        createTableChat()
        createTableRecentChat()
    }
    
    func createTableContact() {
        do {
            try db!.run(contacts.create(ifNotExists: true) { table in
                table.column(pk, primaryKey: true)
                table.column(email, unique: true)
                table.column(phone, unique: true)
                table.column(pubkey)
                table.column(sessionKey)
                table.column(unreadCount)
                table.column(myRequest)
                table.column(isAccepted)
                table.column(isGroup)
            })
        } catch {
            print("Unable to create contact table")
        }
    }
    
    func createTableChat() {
        do {
            try db!.run(chats.create(ifNotExists: true) { table in
                table.column(pk, primaryKey: true)
                table.column(id, unique: true)
                table.column(body)
                table.column(contact)
                table.column(myChat)
                table.column(isViewed)
                table.column(inOrder)
                table.column(timestamp)
            })
        } catch {
            print("Unable to create chat table")
        }
    }
    
    func createTableRecentChat() {
        do {
            try db!.run(recentChats.create(ifNotExists: true) { table in
                table.column(pk, primaryKey: true)
                table.column(contact, unique: true)
                table.column(body)
                table.column(myChat)
                table.column(unreadCount)
                table.column(isViewed)
                table.column(timestamp)
            })
        } catch {
            print("Unable to create chat table")
        }
    }
    
    func createContact(contact: Contact) -> Int64 {
        do {
            let insert = contacts.insert(
                email <- contact.email,
                phone <- contact.phone,
                pubkey <- contact.pubkey,
                sessionKey <- contact.sessionKey,
                unreadCount <- contact.unreadCount,
                myRequest <- contact.myRequest,
                isAccepted <- contact.isAccepted,
                isGroup <- contact.isGroup
            )
            return try db!.run(insert)
        } catch {
            print("Query fail, create contact")
            print("Error create contact : \(error) : \(contact)")
        }
        
        return -1
    }
    
    func markContactAccepted(_phone: String) -> Bool {
        do {
            let u = contacts.filter(phone == _phone)
            let update = u.update([isAccepted <- true])
            if try db!.run(update) > 0 {
                return true
            }
        } catch {
            print("Update failed: \(error)")
        }
        
        return false
    }

    func updateContactPublicKey(_phone: String, _pubkey: String) -> Bool {
        do {
            let u = contacts.filter(phone == _phone)
            let update = u.update([pubkey <- _pubkey])
            if try db!.run(update) > 0 {
                return true
            }
        } catch {
            print("Update failed: \(error)")
        }
        
        return false
    }

    func updateContactSessionKey(_phone: String, _seesionKey: String) -> Bool {
        do {
            let u = contacts.filter(phone == _phone)
            let update = u.update([sessionKey <- _seesionKey])
            if try db!.run(update) > 0 {
                return true
            }
        } catch {
            print("Update failed: \(error)")
        }
        
        return false
    }
    
    func getContacts() -> [Contact] {
        var l = [Contact]()
        let q = self.contacts
        do {
            for i in try db!.prepare(q) {
                let e = i[email]
                let p = i[phone]
                let k = i[pubkey]
                let s = i[sessionKey]
                let u = i[unreadCount]
                let mr = i[myRequest]
                let ac = i[isAccepted]
                let g = i[isGroup]
                
                var a = Contact(email: e, phone: p, pubkey: k, sessionKey: s, unreadCount: u, myRequest: mr, isAccepted: ac)
                if !g {
                    let x = PhoneBookUtil.instance.getContact(phone: p)
                    let o = x == nil ? p : x!.firstName + " " + x!.lastName
                    
                    a.email = o
                    a.thumbnail = x != nil ? x?.thumbnail : nil
                    a.image = x != nil ? x?.image : nil
                }
                
                l.append(a)
            }
        } catch {
            print("Query fail, get contacts")
        }
        
        return l
    }
    
    func getContacts(_isAccepted: Bool) -> [Contact] {
        var l = [Contact]()
        let q = self.contacts.filter(isAccepted == _isAccepted)
        do {
            for i in try db!.prepare(q) {
                let e = i[email]
                let p = i[phone]
                let k = i[pubkey]
                let s = i[sessionKey]
                let u = i[unreadCount]
                let mr = i[myRequest]
                let ac = i[isAccepted]
                let g = i[isGroup]
                let x = PhoneBookUtil.instance.getContact(phone: p)
                let o = x == nil ? p : x!.firstName + " " + x!.lastName
                
                var a = Contact(email: e, phone: p, pubkey: k, sessionKey: s, unreadCount: u, myRequest: mr, isAccepted: ac)
                if !g {
                    let x = PhoneBookUtil.instance.getContact(phone: p)
                    let o = x == nil ? p : x!.firstName + " " + x!.lastName
                    
                    a.email = o
                    a.thumbnail = x != nil ? x?.thumbnail : nil
                    a.image = x != nil ? x?.image : nil
                }
                
                l.append(a)
            }
        } catch {
            print("Query fail, get contacts")
        }
        
        return l
    }
    
    func getContact(_phone: String) -> Contact? {
        let q = contacts.filter(phone == _phone)
        
        do {
            let i = try db!.pluck(q)
            if i != nil {
                let e = i![email]
                let p = i![phone]
                let k = i![pubkey]
                let s = i![sessionKey]
                let u = i![unreadCount]
                let mr = i![myRequest]
                let ac = i![isAccepted]
                let g = i![isGroup]
                
                var c = Contact(email: e, phone: p, pubkey: k, sessionKey: s, unreadCount: u, myRequest: mr, isAccepted: ac)
                if !g {
                    let x = PhoneBookUtil.instance.getContact(phone: p)
                    let o = x == nil ? p : x!.firstName + " " + x!.lastName
                    
                    c.email = o
                    c.thumbnail = x != nil ? x?.thumbnail : nil
                    c.image = x != nil ? x?.image : nil
                }
                
                return c
            }
        } catch {
            print("Query fail, get contact")
        }
        
        return nil
    }
    
    func updateUnreadCount(_contact: String, count: Int, reset: Bool) -> Bool {
        do {
            let update1 = "UPDATE contacts SET unread_count = ? WHERE phone = ?"
            let update2 = "UPDATE contacts SET unread_count = unread_count + ? WHERE phone = ?"
            try db!.run(reset ? update1 : update2, count, _contact)
            
            return true
        } catch {
            print("Update failed: \(error)")
        }
        
        return false
    }
    
    func deleteContact(_phone: String) -> Bool {
        do {
            let u = contacts.filter(phone == _phone)
            try db!.run(u.delete())
            
            return true
        } catch {
            print("Delete failed")
        }
        
        return false
    }
    
    func createChat(chat: Chat) -> Int64 {
        do {
            let insert = chats.insert(
                id <- chat.uid,
                body <- chat.body,
                contact <- chat.contact.phone,
                myChat <- chat.myChat,
                isViewed <- chat.viewed,
                inOrder <- chat.inOrder,
                timestamp <- chat.timestamp
            )
            
            //_ = self.updateChatInOrder(chat: chat)
            
            return try db!.run(insert)
        } catch {
            print("error create chat: \(error)")
        }
        
        return -1
    }
    
    func getChats(_phone: String) -> [Chat] {
        var l = [Chat]()
        do {
            var ct = self.getContact(_phone: _phone)
            let c = PhoneBookUtil.instance.getContact(phone: _phone)
            ct!.email = c == nil ? _phone : c!.firstName + " " + c!.lastName

            let q = chats.filter(contact == _phone).order(timestamp.desc)
            for i in try db!.prepare(q) {
                let x = i[id]
                let b = i[body]
                let t = i[timestamp]
                let m = i[myChat]
                let o = i[inOrder]
                let chat = Chat(uid: x, body: b, timestamp: t, contact: ct!, myChat: m, inOrder: o)
                l.append(chat)
            }
        } catch {
            print("Query fail, get chats")
        }
        
        return l
    }
    
    func getRecentChats() -> [Chat] {
        var l = [Chat]()
        do {
            let q = chats
                .select(pk.max, id, body, timestamp, contact, myChat)
                .group(contact)
                .order(timestamp.desc)
            for i in try db!.prepare(q) {
                let x = i[id]
                let b = i[body]
                let c = i[contact]
                let t = i[timestamp]
                let m = i[myChat]
                var ct = self.getContact(_phone: c)
                let o = PhoneBookUtil.instance.getContact(phone: c)
                ct!.email = o == nil ? c : o!.firstName + " " + o!.lastName
                let chat = Chat(uid: x, body: b.replacingOccurrences(of: "\n", with: ""), timestamp: t, contact: ct!, myChat: m)
                l.append(chat)
            }
        } catch {
            print("Query fail, get chats")
        }
        
        return l
    }
    
    func markChatViewed(_id: String) -> Bool {
        do {
            let u = chats.filter(id == _id)
            let update = u.update([isViewed <- true])
            if try db!.run(update) > 0 {
                return true
            }
        } catch {
            print("Update failed: \(error)")
        }
        
        return false
    }
    
    func deleteChat(_uid: String) -> Bool {
        do {
            let u = chats.filter(id == _uid)
            try db!.run(u.delete())
            
            return true
        } catch {
            print("Delete failed")
        }
        
        return false
    }
    
    func deleteChatsOfContact(_phone: String) -> Bool {
        do {
            let filter = chats.filter(contact == _phone)
            try db!.run(filter.delete())
            
            return true
        } catch {
            print("Delete failed")
        }
        
        return false
    }
    
    func updateChatInOrder(chat: Chat) -> Bool {
        do {
            // get previous chat
            let q = chats
                .filter(contact == chat.contact.phone)
                .select(pk.max, id, body, timestamp, contact, myChat)
                .order(timestamp.desc)
            for i in try db!.prepare(q) {
                let x = i[id]
                let t = i[timestamp]
                let m = i[myChat]
                
                // update only for same type
                if m == chat.myChat && DateUtil.instance.isInOrder(t1: chat.timestamp, t2: t){
                    let update = "UPDATE chats SET in_order = ? WHERE id = ?"
                    try db!.run(update, x)
                    
                    return true
                }
            }
        } catch {
            print("Update chat inorder failed")
        }
        
        return false
    }
    
    func createRecentChat(chat: Chat) -> Int64 {
        do {
            let insert = recentChats.insert(
                contact <- chat.contact.email,
                body <- chat.body,
                myChat <- chat.myChat,
                unreadCount <- 0,
                isViewed <- false,
                timestamp <- chat.timestamp
            )
            try db!.run(insert)
        } catch {
            print("error create recent chat: \(error)")
        }
        
        return -1
    }
    
    func getRecents() -> [Chat] {
        var l = [Chat]()
        do {
            let q = self.recentChats
            for i in try db!.prepare(q) {
                let x = i[id]
                let c = i[contact]
                let b = i[body]
                let t = i[timestamp]
                let m = i[myChat]
                let ct = self.getContact(_phone: c)
                let chat = Chat(uid: x, body: b, timestamp: t, contact: ct!, myChat: m)
                l.append(chat)
            }
        } catch {
            print("Query fail, get chats")
        }
        
        return l
    }
    
    func markRecentChatViewed(_contact: String) -> Bool {
        do {
            let u = recentChats.filter(contact == _contact)
            let update = u.update([isViewed <- true])
            if try db!.run(update) > 0 {
                return true
            }
        } catch {
            print("Update failed: \(error)")
        }
        
        return false
    }
    
    func deleteRecentChat(_contact: String) -> Bool {
        do {
            let u = recentChats.filter(contact == _contact)
            try db!.run(u.delete())
            
            return true
        } catch {
            print("Delete failed")
        }
        
        return false
    }
    
}


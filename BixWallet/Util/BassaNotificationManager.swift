import UserNotifications
import SwiftUI

struct BassaNotification {
    var id: String
    var title: String
    var body: String
}

class BassaNotificationManager {
    
    @EnvironmentObject
    var recentChatObserver: RecentChatObserver
    
    static let selfieNotification = NSNotification.Name("SELFIE")
    static let selfieCallNotification = NSNotification.Name("SELFIE_CALL")
    static let messageNotification = NSNotification.Name("MESSAGE")
    
    var notifications = [BassaNotification]()
    static let instance = BassaNotificationManager()
    
    func requestPermission() -> Void {
        UNUserNotificationCenter
            .current()
            .requestAuthorization(options: [.alert, .badge, .alert]) { granted, error in
                if granted == true && error == nil {
                    // We have permission!
                }
            }
    }
    
    func postNotification(notification: BassaNotification) {
        let content = UNMutableNotificationContent()
        content.title = notification.title
        content.body = notification.body
        content.sound = UNNotificationSound.default
        
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 1, repeats: false)
        let request = UNNotificationRequest(identifier: UUID().uuidString, content: content, trigger: trigger)
        
        UNUserNotificationCenter.current().add(request)
        setBadge(count: 1)
    }
    
    func handleBassaMessage(jsonData: String) {
        do {
            // parse JSON
            let jsonData = Data(jsonData.utf8)
            let dict = try JSONSerialization.jsonObject(with: jsonData, options: []) as? [String: String]
            
            // get sender and body
            let uid = dict?["uid"]
            let sender = dict?["sender"]
            let body = dict?["body"]
            let type = dict?["type"]
            
            if (type == "SELFIE_CALL") {
                // selfie call
            } else if(type == "SELFIE") {
                // selfie
            } else if(type == "FRIEND_REQUEST") {
                var senderContact = BixDb.instance.getContact(_phone: sender!)
                if senderContact == nil {
                    // contact should not exists 
                    // create contact
                    let pc = PhoneBookUtil.instance.getContact(phone: sender!)
                    let name = pc == nil ? sender!: pc!.firstName + " " + pc!.lastName
                    senderContact = Contact(email: name, phone: sender!, myRequest: false, isAccepted: false)
                    _ = BixDb.instance.createContact(contact: senderContact!)
                }
            } else if(type == "ACCEPT_REQUEST") {
                var senderContact = BixDb.instance.getContact(_phone: sender!)
                if senderContact != nil {
                    // accept reqiest, contact should be here
                    let pc = PhoneBookUtil.instance.getContact(phone: sender!)
                    let name = pc == nil ? sender!: pc!.firstName + " " + pc!.lastName
                    senderContact = Contact(email: name, phone: sender!, myRequest: false, isAccepted: false)
                    _ = BixDb.instance.markContactAccepted(_phone: sender!)
                }
            } else {
                var senderContact = BixDb.instance.getContact(_phone: sender!)
                if senderContact != nil {
                    // contact should be here
                    // update unread count
                    //senderContact!.unreadCount = senderContact!.unreadCount + 1
                    //_ = BixDb.instance.updateUnreadCount(_contact: senderContact!.phone, count: 1, reset: false)
                    
                    // save chat
                    //let chat = Chat(uid: uid!, body: body!, timestamp:DateUtil.instance.getTimestamp(), contact: senderContact!, myChat: false)
                    //_ = BixDb.instance.createChat(chat: chat)
                }
            }
            setBadge(count: 1)
        } catch {
            print(error.localizedDescription)
        }
    }
    
    func setBadge(count: Int) {
        UIApplication.shared.applicationIconBadgeNumber = count
    }
    
    func publishNotification(message: Message, notification: NSNotification.Name) {
        NotificationCenter.default.post(name: notification, object: nil, userInfo: ["MESSAGE": message])
    }
}

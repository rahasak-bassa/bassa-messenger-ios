import Foundation

import SwiftyRSA
import CryptoSwift

class CryptoUtil {
    
    static let instance = CryptoUtil()
    
    static let AES_KEY_LEN = 16
    static let AES_IV = "gqLOHUioQ0QjhuvI"
    
    func initKeys() -> Bool {
        if (PreferenceUtil.instance.get(key: PreferenceUtil.PUBLIC_KEY).isEmpty) {
            do {
                // gegnerate key pair
                let keyPair = try SwiftyRSA.generateRSAKeyPair(sizeInBits: 1024)
                let publicKey = keyPair.publicKey
                let privateKey = keyPair.privateKey
                
                // save in user defaults
                PreferenceUtil.instance.put(key: PreferenceUtil.PUBLIC_KEY, value: try publicKey.base64String())
                PreferenceUtil.instance.put(key: PreferenceUtil.PRIVATE_KEY, value: try privateKey.base64String())
            } catch {
                print("error generating keys")
                
                return false
            }
            
            return true
        }
        
        // already setup
        return true
    }
    
    func sign(payload: String) -> String {
        do {
            // get private key
            let privateKey = try PrivateKey(base64Encoded: PreferenceUtil.instance.get(key: PreferenceUtil.PRIVATE_KEY))
            
            // format payload
            let fPaylod = payload.replacingOccurrences(of: " ", with: "")
                .replacingOccurrences(of: "\n", with: "")
                .replacingOccurrences(of: "\r", with: "")
            
            // sign
            let clear = try ClearMessage(string: fPaylod, using: .utf8)
            let signature = try clear.signed(with: privateKey, digestType: .sha256)
            return signature.data.base64EncodedString()
        } catch {
            print("Error signing")
        }
        
        return ""
    }

    func verify(payload: String, signature: String) -> Bool {
        do {
            // get public key
            let publicKey = try PublicKey(base64Encoded: PreferenceUtil.instance.get(key: PreferenceUtil.PUBLIC_KEY))

            // format payload
            let fPaylod = payload.replacingOccurrences(of: " ", with: "")
                .replacingOccurrences(of: "\n", with: "")
                .replacingOccurrences(of: "\r", with: "")

            // decode base64 payload
            let digsig = try Signature(base64Encoded: signature)
            let clear = try ClearMessage(string: fPaylod, using: .utf8)
            
            return try clear.verify(with: publicKey, signature: digsig, digestType: .sha256)
        } catch {
            print("Error signing")
        }
        
        return false
    }
    
    func encryptRSA(payload: String, keystr: String) -> String? {
        do {
            // get public key
            let publicKey = try PublicKey(base64Encoded: keystr)
            
            // encrypt
            let msg = try ClearMessage(string: payload, using: .utf8)
            let encrypted = try msg.encrypted(with: publicKey, padding: .PKCS1)
            
            return encrypted.base64String
        } catch {
            print("Error signing")
        }
        
        return nil
    }
    
    func decryptRSA(payload: String) -> String? {
        do {
            // get private key
            let privateKey = try PrivateKey(base64Encoded: PreferenceUtil.instance.get(key: PreferenceUtil.PRIVATE_KEY))
            
            // decrypt
            let encrypted = try EncryptedMessage(base64Encoded: payload)
            let msg = try encrypted.decrypted(with: privateKey, padding: .PKCS1)

            return try msg.string(encoding: .utf8)
        } catch {
            print("Error signing")
        }
        
        return nil
    }
    
    func getAESKey() -> String {
        let letters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        return String((0..<CryptoUtil.AES_KEY_LEN).map{ _ in letters.randomElement()! })
    }
    
    func encrypAES(key: String, payload: String) -> String? {
        do {
            let aes = try AES(key: key, iv: CryptoUtil.AES_IV, padding: .pkcs7) // aes128
            let ciphertext = try aes.encrypt(Array(payload.utf8))
            return ciphertext.toBase64()
        } catch {
            print("Error: \(error)")
        }
        
        return nil
    }
    
    func decrypAES(key: String, payload: String) -> String? {
        do {
            let aes = try AES(key: key, iv: CryptoUtil.AES_IV, padding: .pkcs7) // aes128
            let ciphertext = Data(base64Encoded: payload)
            let decryptedBytes = try aes.decrypt(ciphertext!.bytes)
            
            return String(bytes: decryptedBytes, encoding: .utf8)
        } catch {
            print("Error: \(error)")
        }
        
        return nil
    }
    
}


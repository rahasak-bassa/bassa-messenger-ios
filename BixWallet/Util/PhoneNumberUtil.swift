import Foundation
import Contacts
import PhoneNumberKit

class PhoneNumberUtil {
    
    static let instance = PhoneNumberUtil()
    private var phoneNumberKit = PhoneNumberKit()
    
    func internationalize(phone: String) -> String? {
        do {
            return self.phoneNumberKit.format(try self.phoneNumberKit.parse(phone), toType: .international)
                .replacingOccurrences(of: " ", with: "")
                .replacingOccurrences(of: "-", with: "")
        } catch {
            return nil
        }
    }
    
}

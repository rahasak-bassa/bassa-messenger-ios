import Foundation

import SwiftSocket

class BixComm {
    
    static let instance = BixComm()
    
    func postMsg(bixReq: String) -> String? {
        var tcpUrl:String?

        #if DEBUG
            tcpUrl = "96.255.175.57"
            //tcpUrl = "96.255.175.53"
        #else
            tcpUrl = "96.255.175.57" // bix prod
            //tcpUrl = "96.255.175.53"
        #endif
        
        let client = TCPClient(address: tcpUrl!, port: 23648) // 23648 register parneter request
        print("send bix request --- " + bixReq)
        
        switch client.connect(timeout: 10) {
        case .success:
            switch client.send(string: bixReq + "\r\n") {
            case .success:
                // read all data from socket
                var data = [UInt8]()
                while (true) {
                    guard let resp = client.read(1024 * 6, timeout: 60) else {
                        break
                    }
                    data += resp
                }
                
                if data.isEmpty {
                    client.close()
                    return nil
                }
                
                if let response = String(bytes: data, encoding: .utf8) {
                    print("got bix response --- " + response)
                    
                    client.close()
                    return response
                }
            case .failure(let error):
                print("erro1 \(error)")
                
                client.close()
                return nil
            }
        case .failure(let error):
            print("erro2 \(error)")
            
            client.close()
            return nil
        }
        
        return nil
    }
    
}

//
// DO NOT EDIT.
//
// Generated by the protocol buffer compiler.
// Source: messagner.proto
//

//
// Copyright 2018, gRPC Authors All rights reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
import Dispatch
import Foundation
import SwiftGRPC
import SwiftProtobuf

internal protocol BassaServiceRegisterDeviceCall: ClientCallUnary {}

fileprivate final class BassaServiceRegisterDeviceCallBase: ClientCallUnaryBase<Device, Reply>, BassaServiceRegisterDeviceCall {
  override class var method: String { return "/BassaService/RegisterDevice" }
}

internal protocol BassaServiceFindDeviceCall: ClientCallUnary {}

fileprivate final class BassaServiceFindDeviceCallBase: ClientCallUnaryBase<DeviceRequest, Device>, BassaServiceFindDeviceCall {
  override class var method: String { return "/BassaService/FindDevice" }
}

internal protocol BassaServiceSendMessageCall: ClientCallUnary {}

fileprivate final class BassaServiceSendMessageCallBase: ClientCallUnaryBase<Message, Reply>, BassaServiceSendMessageCall {
  override class var method: String { return "/BassaService/SendMessage" }
}

internal protocol BassaServiceFetchMessagesCall: ClientCallUnary {}

fileprivate final class BassaServiceFetchMessagesCallBase: ClientCallUnaryBase<FetchRequest, FetchResponse>, BassaServiceFetchMessagesCall {
  override class var method: String { return "/BassaService/FetchMessages" }
}

internal protocol BassaServiceSendAwaCall: ClientCallUnary {}

fileprivate final class BassaServiceSendAwaCallBase: ClientCallUnaryBase<Message, Reply>, BassaServiceSendAwaCall {
  override class var method: String { return "/BassaService/SendAwa" }
}

internal protocol BassaServiceStreamMessageCall: ClientCallServerStreaming {
  /// Do not call this directly, call `receive()` in the protocol extension below instead.
  func _receive(timeout: DispatchTime) throws -> Message?
  /// Call this to wait for a result. Nonblocking.
  func receive(completion: @escaping (ResultOrRPCError<Message?>) -> Void) throws
}

internal extension BassaServiceStreamMessageCall {
  /// Call this to wait for a result. Blocking.
  func receive(timeout: DispatchTime = .distantFuture) throws -> Message? { return try self._receive(timeout: timeout) }
}

fileprivate final class BassaServiceStreamMessageCallBase: ClientCallServerStreamingBase<Connect, Message>, BassaServiceStreamMessageCall {
  override class var method: String { return "/BassaService/StreamMessage" }
}


/// Instantiate BassaServiceServiceClient, then call methods of this protocol to make API calls.
internal protocol BassaServiceService: ServiceClient {
  /// Synchronous. Unary.
  func registerDevice(_ request: Device, metadata customMetadata: Metadata) throws -> Reply
  /// Asynchronous. Unary.
  @discardableResult
  func registerDevice(_ request: Device, metadata customMetadata: Metadata, completion: @escaping (Reply?, CallResult) -> Void) throws -> BassaServiceRegisterDeviceCall

  /// Synchronous. Unary.
  func findDevice(_ request: DeviceRequest, metadata customMetadata: Metadata) throws -> Device
  /// Asynchronous. Unary.
  @discardableResult
  func findDevice(_ request: DeviceRequest, metadata customMetadata: Metadata, completion: @escaping (Device?, CallResult) -> Void) throws -> BassaServiceFindDeviceCall

  /// Synchronous. Unary.
  func sendMessage(_ request: Message, metadata customMetadata: Metadata) throws -> Reply
  /// Asynchronous. Unary.
  @discardableResult
  func sendMessage(_ request: Message, metadata customMetadata: Metadata, completion: @escaping (Reply?, CallResult) -> Void) throws -> BassaServiceSendMessageCall

  /// Synchronous. Unary.
  func fetchMessages(_ request: FetchRequest, metadata customMetadata: Metadata) throws -> FetchResponse
  /// Asynchronous. Unary.
  @discardableResult
  func fetchMessages(_ request: FetchRequest, metadata customMetadata: Metadata, completion: @escaping (FetchResponse?, CallResult) -> Void) throws -> BassaServiceFetchMessagesCall

  /// Synchronous. Unary.
  func sendAwa(_ request: Message, metadata customMetadata: Metadata) throws -> Reply
  /// Asynchronous. Unary.
  @discardableResult
  func sendAwa(_ request: Message, metadata customMetadata: Metadata, completion: @escaping (Reply?, CallResult) -> Void) throws -> BassaServiceSendAwaCall

  /// Asynchronous. Server-streaming.
  /// Send the initial message.
  /// Use methods on the returned object to get streamed responses.
  func streamMessage(_ request: Connect, metadata customMetadata: Metadata, completion: ((CallResult) -> Void)?) throws -> BassaServiceStreamMessageCall

}

internal extension BassaServiceService {
  /// Synchronous. Unary.
  func registerDevice(_ request: Device) throws -> Reply {
    return try self.registerDevice(request, metadata: self.metadata)
  }
  /// Asynchronous. Unary.
  @discardableResult
  func registerDevice(_ request: Device, completion: @escaping (Reply?, CallResult) -> Void) throws -> BassaServiceRegisterDeviceCall {
    return try self.registerDevice(request, metadata: self.metadata, completion: completion)
  }

  /// Synchronous. Unary.
  func findDevice(_ request: DeviceRequest) throws -> Device {
    return try self.findDevice(request, metadata: self.metadata)
  }
  /// Asynchronous. Unary.
  @discardableResult
  func findDevice(_ request: DeviceRequest, completion: @escaping (Device?, CallResult) -> Void) throws -> BassaServiceFindDeviceCall {
    return try self.findDevice(request, metadata: self.metadata, completion: completion)
  }

  /// Synchronous. Unary.
  func sendMessage(_ request: Message) throws -> Reply {
    return try self.sendMessage(request, metadata: self.metadata)
  }
  /// Asynchronous. Unary.
  @discardableResult
  func sendMessage(_ request: Message, completion: @escaping (Reply?, CallResult) -> Void) throws -> BassaServiceSendMessageCall {
    return try self.sendMessage(request, metadata: self.metadata, completion: completion)
  }

  /// Synchronous. Unary.
  func fetchMessages(_ request: FetchRequest) throws -> FetchResponse {
    return try self.fetchMessages(request, metadata: self.metadata)
  }
  /// Asynchronous. Unary.
  @discardableResult
  func fetchMessages(_ request: FetchRequest, completion: @escaping (FetchResponse?, CallResult) -> Void) throws -> BassaServiceFetchMessagesCall {
    return try self.fetchMessages(request, metadata: self.metadata, completion: completion)
  }

  /// Synchronous. Unary.
  func sendAwa(_ request: Message) throws -> Reply {
    return try self.sendAwa(request, metadata: self.metadata)
  }
  /// Asynchronous. Unary.
  @discardableResult
  func sendAwa(_ request: Message, completion: @escaping (Reply?, CallResult) -> Void) throws -> BassaServiceSendAwaCall {
    return try self.sendAwa(request, metadata: self.metadata, completion: completion)
  }

  /// Asynchronous. Server-streaming.
  func streamMessage(_ request: Connect, completion: ((CallResult) -> Void)?) throws -> BassaServiceStreamMessageCall {
    return try self.streamMessage(request, metadata: self.metadata, completion: completion)
  }

}

internal final class BassaServiceServiceClient: ServiceClientBase, BassaServiceService {
  /// Synchronous. Unary.
  internal func registerDevice(_ request: Device, metadata customMetadata: Metadata) throws -> Reply {
    return try BassaServiceRegisterDeviceCallBase(channel)
      .run(request: request, metadata: customMetadata)
  }
  /// Asynchronous. Unary.
  @discardableResult
  internal func registerDevice(_ request: Device, metadata customMetadata: Metadata, completion: @escaping (Reply?, CallResult) -> Void) throws -> BassaServiceRegisterDeviceCall {
    return try BassaServiceRegisterDeviceCallBase(channel)
      .start(request: request, metadata: customMetadata, completion: completion)
  }

  /// Synchronous. Unary.
  internal func findDevice(_ request: DeviceRequest, metadata customMetadata: Metadata) throws -> Device {
    return try BassaServiceFindDeviceCallBase(channel)
      .run(request: request, metadata: customMetadata)
  }
  /// Asynchronous. Unary.
  @discardableResult
  internal func findDevice(_ request: DeviceRequest, metadata customMetadata: Metadata, completion: @escaping (Device?, CallResult) -> Void) throws -> BassaServiceFindDeviceCall {
    return try BassaServiceFindDeviceCallBase(channel)
      .start(request: request, metadata: customMetadata, completion: completion)
  }

  /// Synchronous. Unary.
  internal func sendMessage(_ request: Message, metadata customMetadata: Metadata) throws -> Reply {
    return try BassaServiceSendMessageCallBase(channel)
      .run(request: request, metadata: customMetadata)
  }
  /// Asynchronous. Unary.
  @discardableResult
  internal func sendMessage(_ request: Message, metadata customMetadata: Metadata, completion: @escaping (Reply?, CallResult) -> Void) throws -> BassaServiceSendMessageCall {
    return try BassaServiceSendMessageCallBase(channel)
      .start(request: request, metadata: customMetadata, completion: completion)
  }

  /// Synchronous. Unary.
  internal func fetchMessages(_ request: FetchRequest, metadata customMetadata: Metadata) throws -> FetchResponse {
    return try BassaServiceFetchMessagesCallBase(channel)
      .run(request: request, metadata: customMetadata)
  }
  /// Asynchronous. Unary.
  @discardableResult
  internal func fetchMessages(_ request: FetchRequest, metadata customMetadata: Metadata, completion: @escaping (FetchResponse?, CallResult) -> Void) throws -> BassaServiceFetchMessagesCall {
    return try BassaServiceFetchMessagesCallBase(channel)
      .start(request: request, metadata: customMetadata, completion: completion)
  }

  /// Synchronous. Unary.
  internal func sendAwa(_ request: Message, metadata customMetadata: Metadata) throws -> Reply {
    return try BassaServiceSendAwaCallBase(channel)
      .run(request: request, metadata: customMetadata)
  }
  /// Asynchronous. Unary.
  @discardableResult
  internal func sendAwa(_ request: Message, metadata customMetadata: Metadata, completion: @escaping (Reply?, CallResult) -> Void) throws -> BassaServiceSendAwaCall {
    return try BassaServiceSendAwaCallBase(channel)
      .start(request: request, metadata: customMetadata, completion: completion)
  }

  /// Asynchronous. Server-streaming.
  /// Send the initial message.
  /// Use methods on the returned object to get streamed responses.
  internal func streamMessage(_ request: Connect, metadata customMetadata: Metadata, completion: ((CallResult) -> Void)?) throws -> BassaServiceStreamMessageCall {
    return try BassaServiceStreamMessageCallBase(channel)
      .start(request: request, metadata: customMetadata, completion: completion)
  }

}

/// To build a server, implement a class that conforms to this protocol.
/// If one of the methods returning `ServerStatus?` returns nil,
/// it is expected that you have already returned a status to the client by means of `session.close`.
internal protocol BassaServiceProvider: ServiceProvider {
  func registerDevice(request: Device, session: BassaServiceRegisterDeviceSession) throws -> Reply
  func findDevice(request: DeviceRequest, session: BassaServiceFindDeviceSession) throws -> Device
  func sendMessage(request: Message, session: BassaServiceSendMessageSession) throws -> Reply
  func fetchMessages(request: FetchRequest, session: BassaServiceFetchMessagesSession) throws -> FetchResponse
  func sendAwa(request: Message, session: BassaServiceSendAwaSession) throws -> Reply
  func streamMessage(request: Connect, session: BassaServiceStreamMessageSession) throws -> ServerStatus?
}

extension BassaServiceProvider {
  internal var serviceName: String { return "BassaService" }

  /// Determines and calls the appropriate request handler, depending on the request's method.
  /// Throws `HandleMethodError.unknownMethod` for methods not handled by this service.
  internal func handleMethod(_ method: String, handler: Handler) throws -> ServerStatus? {
    switch method {
    case "/BassaService/RegisterDevice":
      return try BassaServiceRegisterDeviceSessionBase(
        handler: handler,
        providerBlock: { try self.registerDevice(request: $0, session: $1 as! BassaServiceRegisterDeviceSessionBase) })
          .run()
    case "/BassaService/FindDevice":
      return try BassaServiceFindDeviceSessionBase(
        handler: handler,
        providerBlock: { try self.findDevice(request: $0, session: $1 as! BassaServiceFindDeviceSessionBase) })
          .run()
    case "/BassaService/SendMessage":
      return try BassaServiceSendMessageSessionBase(
        handler: handler,
        providerBlock: { try self.sendMessage(request: $0, session: $1 as! BassaServiceSendMessageSessionBase) })
          .run()
    case "/BassaService/FetchMessages":
      return try BassaServiceFetchMessagesSessionBase(
        handler: handler,
        providerBlock: { try self.fetchMessages(request: $0, session: $1 as! BassaServiceFetchMessagesSessionBase) })
          .run()
    case "/BassaService/SendAwa":
      return try BassaServiceSendAwaSessionBase(
        handler: handler,
        providerBlock: { try self.sendAwa(request: $0, session: $1 as! BassaServiceSendAwaSessionBase) })
          .run()
    case "/BassaService/StreamMessage":
      return try BassaServiceStreamMessageSessionBase(
        handler: handler,
        providerBlock: { try self.streamMessage(request: $0, session: $1 as! BassaServiceStreamMessageSessionBase) })
          .run()
    default:
      throw HandleMethodError.unknownMethod
    }
  }
}

internal protocol BassaServiceRegisterDeviceSession: ServerSessionUnary {}

fileprivate final class BassaServiceRegisterDeviceSessionBase: ServerSessionUnaryBase<Device, Reply>, BassaServiceRegisterDeviceSession {}

internal protocol BassaServiceFindDeviceSession: ServerSessionUnary {}

fileprivate final class BassaServiceFindDeviceSessionBase: ServerSessionUnaryBase<DeviceRequest, Device>, BassaServiceFindDeviceSession {}

internal protocol BassaServiceSendMessageSession: ServerSessionUnary {}

fileprivate final class BassaServiceSendMessageSessionBase: ServerSessionUnaryBase<Message, Reply>, BassaServiceSendMessageSession {}

internal protocol BassaServiceFetchMessagesSession: ServerSessionUnary {}

fileprivate final class BassaServiceFetchMessagesSessionBase: ServerSessionUnaryBase<FetchRequest, FetchResponse>, BassaServiceFetchMessagesSession {}

internal protocol BassaServiceSendAwaSession: ServerSessionUnary {}

fileprivate final class BassaServiceSendAwaSessionBase: ServerSessionUnaryBase<Message, Reply>, BassaServiceSendAwaSession {}

internal protocol BassaServiceStreamMessageSession: ServerSessionServerStreaming {
  /// Send a message to the stream. Nonblocking.
  func send(_ message: Message, completion: @escaping (Error?) -> Void) throws
  /// Do not call this directly, call `send()` in the protocol extension below instead.
  func _send(_ message: Message, timeout: DispatchTime) throws

  /// Close the connection and send the status. Non-blocking.
  /// This method should be called if and only if your request handler returns a nil value instead of a server status;
  /// otherwise SwiftGRPC will take care of sending the status for you.
  func close(withStatus status: ServerStatus, completion: (() -> Void)?) throws
}

internal extension BassaServiceStreamMessageSession {
  /// Send a message to the stream and wait for the send operation to finish. Blocking.
  func send(_ message: Message, timeout: DispatchTime = .distantFuture) throws { try self._send(message, timeout: timeout) }
}

fileprivate final class BassaServiceStreamMessageSessionBase: ServerSessionServerStreamingBase<Connect, Message>, BassaServiceStreamMessageSession {}

